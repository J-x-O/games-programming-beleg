using UnityEngine;
using UnityEngine.VFX;
using UnityEngine.VFX.Utility;

namespace Art.ParticleEffects.Extentions {

    [VFXBinder("Physics/Rigidbody Velocity")]
    public class RigidBodyPropertyBinder : VFXBinderBase {
        
        [VFXPropertyBinding("UnityEngine.Vector3")]
        public ExposedProperty _velocityProperty;

        public Rigidbody _target;
        
        public override bool IsValid(VisualEffect component) {
            return _target != null && component.HasVector3(_velocityProperty);
        }

        public override void UpdateBinding(VisualEffect component) {
            
            component.SetVector3(_velocityProperty, _target.velocity);
        }
    }

}