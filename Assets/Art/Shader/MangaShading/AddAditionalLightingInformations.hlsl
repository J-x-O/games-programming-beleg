// https://blog.unity.com/technology/custom-lighting-in-shader-graph-expanding-your-graphs-in-2019

void AddAdditionalLights_float(float MainDiffuse, float3 MainSpecular, float3 MainColor,
    float Smoothness, float3 WorldPosition, float3 WorldNormal, float3 WorldView,
    out float FinalDiffuse, out float3 FinalSpecular, out float3 FinalColor) {
    
    FinalDiffuse = MainDiffuse;
    FinalSpecular = MainSpecular;
    FinalColor = MainColor * (MainDiffuse + MainSpecular);

    #ifndef SHADERGRAPH_PREVIEW
    int lightCount = GetAdditionalLightsCount();
    for (int i = 0; i < lightCount; ++i) {
        Light light = GetAdditionalLight(i, WorldPosition);
        float NdotL = saturate(dot(WorldNormal, light.direction));
        float atten = light.distanceAttenuation * light.shadowAttenuation;
        float thisDiffuse = atten * NdotL;
        float3 thisSpecular = LightingSpecular(thisDiffuse, light.direction, WorldNormal, WorldView, 1, Smoothness);
        FinalDiffuse += thisDiffuse;
        FinalSpecular += thisSpecular;
        FinalColor += light.color * (thisDiffuse + thisSpecular);
    }
    #endif

    float total = FinalDiffuse + FinalSpecular;
    // If no light touches this pixel, set the color to the main light's color
    FinalColor = total <= 0 ? MainColor : FinalColor / total;
}
