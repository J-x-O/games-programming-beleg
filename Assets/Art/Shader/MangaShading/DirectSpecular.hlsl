// https://blog.unity.com/technology/custom-lighting-in-shader-graph-expanding-your-graphs-in-2019

void DirectSpecular_float(float Smoothness, float3 Direction,
    float3 Color, float3 WorldNormal, float3 WorldView, out float3 Out) {

    #ifdef SHADERGRAPH_PREVIEW
        Out = 0;
    #else
        Smoothness = exp2(10 * Smoothness + 1);
        WorldNormal = normalize(WorldNormal);
        WorldView = SafeNormalize(WorldView);
        Out = LightingSpecular(Color, Direction, WorldNormal, WorldView, 1, Smoothness);
    #endif
}
