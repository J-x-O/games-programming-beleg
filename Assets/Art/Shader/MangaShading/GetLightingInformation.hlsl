// https://blog.unity.com/technology/custom-lighting-in-shader-graph-expanding-your-graphs-in-2019

void GetLightingInformation_float(float3 WorldPosition, out float3 Direction, out float3 Color,
    out float DistanceAtten, out float ShadowAtten) {

    #ifdef SHADERGRAPH_PREVIEW
        Direction = half3(0.5, 0.5, 0);
        Color = 1;
        DistanceAtten = 1;
        ShadowAtten = 1;
    #else
    
        #if SHADOWS_SCREEN
            half4 clipPos = TransformWorldToHClip(WorldPosition);
            half4 shadowCoord = ComputeScreenPos(clipPos);
        #else
            float4 shadowCoord = TransformWorldToShadowCoord(WorldPosition);
        #endif
        Light mainLight = GetMainLight(shadowCoord);
        Direction = mainLight.direction;
        Color = mainLight.color;
        DistanceAtten = mainLight.distanceAttenuation;
        ShadowAtten = mainLight.shadowAttenuation;
    #endif
}

