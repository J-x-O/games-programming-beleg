
// Function to calculate pseudo random Offset
inline float2 unity_voronoi_noise_randomVector (float2 UV, float offset) {
    
    float2x2 m = float2x2(15.27, 47.63, 99.41, 89.98);
    UV = frac(sin(mul(UV, m)) * 46839.32);
    return float2(sin(UV.y*+offset)*0.5+0.5, cos(UV.x*offset)*0.5+0.5);
}

inline float distanceToLine(float2 from, float2 linePos1, float2 linePos2) {
    float2 lineDir = linePos2 - linePos1;
    float2 perpDir = float2(lineDir.y, -lineDir.x);
    float2 dirToPos1 = linePos1 - from;
    return abs(dot(normalize(perpDir), dirToPos1));
}

void Voronoi_float(float2 UV, float AngleOffset, float CellDensity,
    out float Out, out float Cells, out float2 CellCenter, out float EdgeDistance) {
    
    float2 globalIndex = floor(UV * CellDensity);         // The int part of the UV-Coord -> (5,1)
    float2 localSamplePos = frac(UV * CellDensity);     // The fraction part of the UV-Coord -> (0.321, 0.842)

    float lowestDistance = 10.0;
    float2 closestCell;
    
    [unroll]
    for(int y1=-1; y1<=1; y1++) {
        [unroll]
        for(int x1=-1; x1<=1; x1++) {

            // Random Offset based on globalPosition => different for every Point
            float2 localIndex = float2(x1,y1);
            float2 offset = unity_voronoi_noise_randomVector(globalIndex + localIndex, AngleOffset);
            float2 localCell = localIndex + offset;
            
            // Calculate Distance and Update Variables if necessary
            // offset is random for every cell, and can there for be used to mark each cell
            float currentDistance = distance(localCell, localSamplePos);
            
            if(currentDistance < lowestDistance) {
                lowestDistance = currentDistance;
                Out = currentDistance;
                Cells = (offset.x + offset.y) / 2; 
                CellCenter = (localCell + globalIndex) / CellDensity;
                closestCell = localCell;
            }
        }
    }

    // Pass2 to find the minimum Distance to Edge
    // https://www.ronja-tutorials.com/post/028-voronoi-noise/

    EdgeDistance = 100.0;
    
    [unroll]
    for(int x2=-1; x2<=1; x2++) {
        [unroll]
        for(int y2=-1; y2<=1; y2++) {

            // same as above, process the current Offset, it will be the same, since its only pseudo random
            float2 localIndex = float2(x2,y2);
            float2 offset = unity_voronoi_noise_randomVector(globalIndex + localIndex, AngleOffset);
            float2 localCell = localIndex + offset;

            // We dont want to process the distance to our own Cell
            float2 differenceToClosestCell = abs(closestCell - localCell);
            bool isClosestCell = length(differenceToClosestCell) < 0.1;
            if(!isClosestCell) {

                // https://www.ronja-tutorials.com/assets/images/posts/028/BorderDistanceExplanation.png
                // We first calculate the black distance to the line, then the Border Distance
                float distanceToCellLine = distanceToLine(localSamplePos, closestCell, localCell);

                float2 center = (closestCell + localCell) * 0.5;
                float distanceToCenter = distance(localSamplePos, center);
                
                float distanceToBorder = sqrt(pow(distanceToCenter, 2) - pow(distanceToCellLine, 2));
                
                EdgeDistance = min(distanceToBorder, EdgeDistance);
            }
        }
    }
}
