using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem.Utilities;

[CreateAssetMenu(menuName = "ScriptableObjects/Audio/Adaptive Music")]
public class AdaptiveMusicAudioEvent : AudioEvent {

    public override float Length => _cachedAverage ??= _clips.Average(clip => clip.length);
    private float? _cachedAverage;
    
    /// <summary> Stores different levels of adaptive sound </summary>
    public ReadOnlyArray<AudioClip> Clips => _clips;
    
    [Tooltip("Stores different levels of adaptive sound")]
    [SerializeField] private AudioClip[] _clips;

    /// <summary> The preferred volume of all levels </summary>
    public float Volume => _volume;
    
    [Tooltip("The preferred volume of all levels")] [Range(0f, 1f)]
    [SerializeField] private float _volume;

    /// <summary> Identifier of this AudioEvent </summary>
    public string AudioName => _audioName;

    [Tooltip("Identifier of this AudioEvent")]
    [SerializeField] private string _audioName;

    /// <summary>Plays the event</summary>
    /// <param name="sources">Array of AudioSources, on which the event is played</param>
    public void Play(AudioSource[] sources) {
        for(int i = 0; i < Clips.Count; i++) {
            sources[i].clip = Clips[i];
            sources[i].volume = Volume;
            sources[i].pitch = 1f;
            sources[i].Play();
        }
    }
}