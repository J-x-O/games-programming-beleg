using UnityEngine;

/// <summary> Parent class for audioevents </summary>
public abstract class AudioEvent : ScriptableObject {

    /// <summary> The length this Event needs to be played from start to finish </summary>
    public abstract float Length { get; }
    
    /// <summary> Stores name of the sound </summary>
    public string AudioName => _audioName;
    
    [Tooltip("Unique Name of the Sound. Used to start it from Script.")]
    [SerializeField] private string _audioName;
    
}

