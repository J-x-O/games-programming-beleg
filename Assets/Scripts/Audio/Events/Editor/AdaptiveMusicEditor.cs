using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Beta.Audio {

	/// <summary> UnityUI-Code. Don't question it, it just works. </summary>
	[CustomEditor(typeof(AdaptiveMusicAudioEvent), true)]
	public class AdaptiveMusicEditor : Editor {

		[SerializeField] private AudioSource[] _previewer;

		public void OnEnable() {
			_previewer = new  AudioSource[((AdaptiveMusicAudioEvent) target).Clips.Count];
			for(int i = 0; i < _previewer.Length; i++) {
				_previewer[i] = EditorUtility
					.CreateGameObjectWithHideFlags("Audio preview", HideFlags.HideAndDontSave, typeof(AudioSource))
					.GetComponent<AudioSource>();
			}
		}

		public void OnDisable() {
            foreach(AudioSource a in _previewer) {
                DestroyImmediate(a.gameObject);
            }
		}

		public override void OnInspectorGUI() {
            //amount = (int)GUI.HorizontalSlider(new Rect(25,150,100,30), amount, 0.0f, 25.0f);
			DrawDefaultInspector();

			EditorGUI.BeginDisabledGroup(serializedObject.isEditingMultipleObjects);
			if (GUILayout.Button("Preview")) {
				((AdaptiveMusicAudioEvent) target).Play(_previewer);
			}

			EditorGUI.EndDisabledGroup();
		}
	}

}
