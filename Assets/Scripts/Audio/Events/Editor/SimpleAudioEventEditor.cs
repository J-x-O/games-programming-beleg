using UnityEngine;
using System.Collections;
using Beta.Audio;
using UnityEditor;

/// <summary> UnityUI-Code. Don't question it, it just works. </summary>
[CustomEditor(typeof(SimpleAudioEvent), true)]
public class SimpleAudioEventEditor : Editor {

	[SerializeField] private AudioSource _previewer;

	public void OnEnable() {
		_previewer = EditorUtility
			.CreateGameObjectWithHideFlags("Audio preview", HideFlags.HideAndDontSave, typeof(AudioSource))
			.GetComponent<AudioSource>();
	}

	public void OnDisable() {
		DestroyImmediate(_previewer.gameObject);
	}

	public override void OnInspectorGUI() {
		DrawDefaultInspector();

		EditorGUI.BeginDisabledGroup(serializedObject.isEditingMultipleObjects);
		if (GUILayout.Button("Preview")) {
			((SimpleAudioEvent) target).Play(_previewer);
		}

		EditorGUI.EndDisabledGroup();
	}
}
