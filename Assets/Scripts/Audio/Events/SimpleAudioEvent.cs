﻿using UnityEngine;

namespace Beta.Audio {
    public abstract class SimpleAudioEvent : AudioEvent {

        /// <summary>Plays the event</summary>
        /// <param name="source">AudioSource, on which the event is played</param>
        public abstract void Play(AudioSource source);
    }
}