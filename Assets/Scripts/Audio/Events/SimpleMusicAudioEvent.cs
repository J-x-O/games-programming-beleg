using UnityEngine;
using System.Collections;
using Beta.Audio;

/// <summary>Stores non-adaptive music events</summary>
/// <author>Nikolas</author>
[CreateAssetMenu(menuName = "ScriptableObjects/Audio/Simple Music")]
public class SimpleMusicAudioEvent : SimpleAudioEvent {

    public override float Length => Clip.length;
    
    /// <summary> The audio clip that will be played </summary>
    public AudioClip Clip => _clip;
    
    [Tooltip("The audio clip that will be played")]
    [SerializeField] private AudioClip _clip;

    /// <summary> The volume the audio clip will be played at </summary>
    public float Volume => _volume;
    
    [Tooltip("The volume the audio clip will be played at")] [Range(0f, 2f)]
    [SerializeField] private float _volume;

    /// <summary>Plays the event</summary>
    /// <param name="source">AudioSource, on which the event is played</param>
    public override void Play(AudioSource source) {

        if (Clip == null) return;

        source.clip = Clip;
        source.volume = Volume;
        source.pitch = 1f;
        source.Play();
    }
}
