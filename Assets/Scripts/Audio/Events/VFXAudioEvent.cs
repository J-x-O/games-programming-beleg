using UnityEngine;
using System.Collections;
using System.Linq;
using Beta.Audio;
using GD.MinMaxSlider;
using Random = UnityEngine.Random;
using UnityEngine.Serialization;

/// <summary>Stores sound events</summary>
/// <author>Nikolas</author>
[CreateAssetMenu(menuName = "ScriptableObjects/Audio/VFX")]
public class VFXAudioEvent : SimpleAudioEvent {

    public override float Length => _cachedAverage ??= _clips.Average(clip => clip.length);
    private float? _cachedAverage;
    
    [FormerlySerializedAs("clips")]
    [Tooltip("All AudioClips for this event, randomly choosen on play")]
    [SerializeField] private AudioClip[] _clips;


    [FormerlySerializedAs("volume")]
    [Tooltip("The minimal and maximal volume, when one Clip is played")]
    [MinMaxSlider(0, 2)]
    [SerializeField] private Vector2 _volume;


    [FormerlySerializedAs("pitch")]
    [Tooltip("The minimal and maximal pitch, when one Clip is played")]
    [MinMaxSlider(0, 2)]
    [SerializeField] public Vector2 _pitch;

    /// <summary>Plays the event</summary>
    /// <param name="source">AudioSource, on which the event is played</param>
    public override void Play(AudioSource source) {

        if (_clips.Length == 0) return;

        source.clip = _clips[Random.Range(0, _clips.Length)];
        source.volume = Random.Range(_volume.x, _volume.y);
        source.pitch = Random.Range(_pitch.x, _pitch.y);
        source.PlayOneShot(source.clip, source.volume);
    }
}