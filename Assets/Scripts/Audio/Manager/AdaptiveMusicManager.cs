using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Beta.Audio;
using UnityEngine;
public class AdaptiveMusicManager : MusicManager<AdaptiveMusicAudioEvent, AdaptiveSoundSourceWrapper> {

    /// <summary>Defines how many levels of music are played</summary>
    public float currentLevel = 0;

    private int _maxLevel;

    protected override void Awake() {
        base.Awake();
        _maxLevel = _musicEvents.Max(musicEvent => musicEvent.Clips.Count);
    }

    protected override AdaptiveSoundSourceWrapper SetupWrapper() {
        AudioSource[] audioSources = Enumerable.Range(0, _maxLevel)
            .Select(index => gameObject.AddComponent<AudioSource>())
            .Where(source => { source.volume = 0; return true; })
            .ToArray();
        return new AdaptiveSoundSourceWrapper(audioSources, _fadeTime);
    }
    
    private void OnDestroy() {
        StopAllCoroutines();
    }
}

