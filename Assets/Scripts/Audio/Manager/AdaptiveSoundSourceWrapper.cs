﻿using System;
using System.Linq;
using Beta.Audio;
using UnityEngine;

public class AdaptiveSoundSourceWrapper : SoundSourceWrapper<AdaptiveMusicAudioEvent> {

    /// <summary> If the <see cref="AudioSource"/> is currently Playing </summary>
    public override bool IsPlaying => _source[0].isPlaying;

    /// <summary> The current Volume of the <see cref="AudioSource"/> </summary>
    public override float Volume => _source[0].volume;

    /// <summary> The <see cref="AudioSource"/>, that is being managed by this wrapper </summary>
    private AudioSource[] _source;

    private float _currentLevel;
    private float _targetLevel;
    private int _eventLayerCount;

    private LTDescr _levelTweenDown;
    private LTDescr _levelTweenUp;

    /// <summary> Default Constructor </summary>
    /// <param name="source"> The <see cref="AudioSource"/>, that is being managed by this wrapper </param>
    /// <param name="fadeDuration"> How long a fade takes, until its done </param>
    public AdaptiveSoundSourceWrapper(AudioSource[] source, float fadeDuration) {
        _source = source;
        _fadeDuration = fadeDuration;
    }

    /// <summary> A Method for Playing and Fading in a MusicEvent </summary>
    /// <param name="musicEvent"> The Music Event that will be played</param>
    public override void Play(AdaptiveMusicAudioEvent musicEvent) {
        for (int i = 0; i < musicEvent.Clips.Count; i++) {
            _source[i].clip = musicEvent.Clips[i];
            _source[i].pitch = 1f;
            _source[i].Play();
        }
        _currentTargetVolume = musicEvent.Volume;
        _eventLayerCount = musicEvent.Clips.Count;
        FadeIn(0, (int) (_eventLayerCount * _currentLevel), ref _fadeTween);
    }

    /// <summary> A method to increase the volume of all wrapped <see cref="AudioSource"/>s </summary>
    /// <param name="leftBound"> Left index of the effected Area </param>
    /// <param name="rightBound"> Right index of the effected Area</param>
    /// <param name="targetVolume"> The value the Volume will be increased to over time </param>
    /// <param name="fadeTween"> The variable where the LeenTween will be saved </param>
    private void FadeIn(int leftBound, int rightBound, ref LTDescr fadeTween) {
        
        // Calculate the affected Sources
        AudioSource[] effectedSources = leftBound < rightBound
            ? _source.Where((source, index) => leftBound < index && index < rightBound).ToArray()
            : _source.Where((source, index) => rightBound < index && index < leftBound).ToArray();
        
        // Cache initial Volumes, calculate the current Progress, upgrade the current targetVolume
        float[] startVolumes =  effectedSources.Select(source => source.volume).ToArray();;
        float startProgress = Mathf.Clamp01(startVolumes.Average() / _currentTargetVolume);

        // Start a new LeanTween to increase the volume of all tracks that need to be faded in over time
        if(fadeTween != null) LeanTween.cancel(fadeTween.uniqueId);
        fadeTween = LeanTween.value(0, 1, _fadeDuration * startProgress)
            .setEaseInSine()
            .setOnUpdate(progress => {
                for (int i = 0; i < effectedSources.Length; i++) {
                    effectedSources[i].volume = startVolumes[i] + progress * (_currentTargetVolume - startVolumes[i]);
                }
            });
    }

    
    /// <summary> Sets the level of the adaptive Sound </summary>
    /// <param name="newLevel"> the new Level we will play at </param>
    public void SetLevel(float newLevel) {
        
        // It is important that we use 2 separate tweens for turning up and down
        // Since we need them running at the same time, if we interrupt a transition and
        // the newLevel is in between the targetLevel and the currentLevel

        // Calculate the highest and the lowest value, which takes active transitions into account
        float highest = Mathf.Max(_currentLevel, _targetLevel);
        float lowest = Mathf.Min(_currentLevel, _targetLevel);
        
        // Update the target Value
        _targetLevel = newLevel;

        // if the newLevel is lower than our highest value, we need to turn all Sources between them down
        if (newLevel < highest) {
            int bound1 = (int) (newLevel * _eventLayerCount);
            int bound2 = (int) (highest * _eventLayerCount);
            FadeOut(bound1, bound2, ref _levelTweenDown);
            _levelTweenDown.setOnComplete(() => _currentLevel = newLevel);
        }

        // if the newLevel is higher than our lowest value, we need to turn all Sources between them up
        if (lowest < newLevel) {
            int bound1 = (int) (lowest * _eventLayerCount);
            int bound2 = (int) (newLevel * _eventLayerCount);
            FadeIn(bound1, bound2, ref _levelTweenUp);
            _levelTweenUp.setOnComplete(() => _currentLevel = newLevel);
        }
    }

    public override void FadeOut() => FadeOut(null);

    /// <summary> A Method that fades out and stops all wrapped <see cref="AudioSource"/>s </summary>
    /// <param name="onFadeEnd"> An action that will be executed as soon as the Fade is over </param>
    public void FadeOut(Action onFadeEnd) {
        FadeOut(0, _source.Length, ref _fadeTween);
        _fadeTween?.setOnComplete(() => onFadeEnd?.Invoke());
    }

    /// <summary> A Method that fades out and stops all wrapped <see cref="AudioSource"/>s </summary>
    /// <param name="leftBound"> Left index of the effected Area </param>
    /// <param name="rightBound"> Right index of the effected Area</param>
    /// <param name="fadeTween"> The variable where the LeenTween will be saved </param>
    private void FadeOut(int leftBound, int rightBound, ref LTDescr fadeTween) {
        
        // Calculate the affected Sources
        AudioSource[] effectedSources = leftBound < rightBound
            ? _source.Where((source, index) => leftBound < index && index < rightBound).ToArray()
            : _source.Where((source, index) => rightBound < index && index < leftBound).ToArray();
        
        // Cache initial Volumes and calculate the overall fade progress
        float[] startVolumes = effectedSources.Select(source => source.volume).ToArray();
        if (Volume == 0 || _currentTargetVolume == 0) return;
        float startProgress = Mathf.Clamp01(startVolumes.Average() / _currentTargetVolume);
        
        // Start the new LeanTween to turn down the Audio over Time
        if(fadeTween != null) LeanTween.cancel(fadeTween.uniqueId);
        fadeTween = LeanTween.value(1, 0, _fadeDuration * startProgress)
            .setEaseInSine()
            .setOnUpdate(progress => {
                for (int i = 0; i < effectedSources.Length; i++) {
                    effectedSources[i].volume = progress * startVolumes[i];
                }
                if (progress <= 0.001f) FreeAudioSource();
            });
    }

    /// <summary> Method to forcefully reset the </summary>
    public override void FreeAudioSource() {
        if(_fadeTween != null) LeanTween.cancel(_fadeTween.uniqueId);
        foreach (AudioSource source in _source) {
            source.Stop();
            source.volume = 0;
        }
        _currentTargetVolume = 0;
    }
}