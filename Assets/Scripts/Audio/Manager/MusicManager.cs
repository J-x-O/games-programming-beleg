﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Beta.Audio {

    public interface MusicManager {
        
        /// <summary> An Event that is being thrown, as soon as the currently playing Song runs out </summary>
        public event Action OnSongOver;
        
        /// <summary> An Event that is being thrown, right before the Song ends, when timeLeft equals the fadeDuration </summary>
        public event Action OnSongAlmostOver;

        /// <summary> Defines how long the blend takes </summary>
        public float FadeTime { get; }
        
        /// <summary> The Names of all AudioEvents </summary>
        public AudioEvent[] AudioEvents { get; }
        
        /// <summary> Starts playing music, starts a blend, if music is already playing </summary>
        /// <param name="target"> name of the music, that is provided by the Event </param>
        /// <returns> If the name was successfully linked to an event and music was started </returns>
        public bool PlayMusic(string target);

        /// <summary> Stops the currently playing Track </summary>
        public void StopMusic();
    }
    
    public abstract class MusicManager<TAudioEvent, TWrapper> : MonoBehaviour, MusicManager
        where TAudioEvent : AudioEvent
        where TWrapper : SoundSourceWrapper<TAudioEvent> {

        public static MusicManager<TAudioEvent, TWrapper> Instance;
        
        /// <summary> An Event that is being thrown, as soon as the currently playing Song runs out </summary>
        public event Action OnSongOver;
        
        /// <summary> An Event that is being thrown, right before the Song ends, when timeLeft equals the fadeDuration </summary>
        public event Action OnSongAlmostOver;
        
        /// <summary> Defines how long the blend takes </summary>
        public float FadeTime => _fadeTime;
        
        [Tooltip("Defines how long the blend takes")]
        [SerializeField] protected float _fadeTime;
        
        /// <summary> All available AudioEvents </summary>
        public AudioEvent[] AudioEvents => _musicEvents;
        
        [Tooltip("The Music that can be played")]
        [SerializeField] protected TAudioEvent[] _musicEvents;

        /// <summary> Cached Music in a Dictionary for efficency </summary>
        protected Dictionary<string, TAudioEvent> _dictionary = new Dictionary<string, TAudioEvent>();

        /// <summary>Stores references to AudioSources</summary>
        protected TWrapper[] _sources = new TWrapper[2];
        
        /// <summary> The LeanTween invoking the Events </summary>
        protected LTDescr _eventInvokeTween;
        
        protected virtual void Awake() {

            // Singleton Pattern
            if (Instance != null) Destroy(gameObject);
            else Instance = this;
            
            // Fill dictionary with the Names of each MusicEvent
            foreach (TAudioEvent s in _musicEvents) _dictionary.Add(s.AudioName, s);

            // Initialize all SoundSources
            for(int i = 0; i < _sources.Length; i++) _sources[i] = SetupWrapper();

            // Keep music playing while changing scenes
            DontDestroyOnLoad(gameObject);
        }

        protected abstract TWrapper SetupWrapper();
        
        /// <summary> Starts playing music, starts blend, if music is already playing </summary>
        /// <param name="target"> name of the music, that is provided by the Event </param>
        /// <returns> If the name was successfully linked to an event and music was started </returns>
        public bool PlayMusic(string target) {
            if(_dictionary.ContainsKey(target)) {
                
                // Fade Out all other Sources
                StopMusic();
                
                // Get an available AudioSource and Play the Event
                TWrapper wrapper = GetAudioSource();
                TAudioEvent audioEvent = _dictionary[target];
                wrapper.Play(audioEvent);

                // Cancel the old Tween and Start a new one, that will invoke the Events at the right time
                if(_eventInvokeTween != null) LeanTween.cancel(_eventInvokeTween.uniqueId);
                _eventInvokeTween = LeanTween.delayedCall(audioEvent.Length - _fadeTime, () => {
                    OnSongAlmostOver?.Invoke();
                    _eventInvokeTween = LeanTween.delayedCall(_fadeTime, () => {
                        OnSongOver?.Invoke();
                    });
                });

                return true;
            }
            Debug.LogWarning("Music " + target + " couldn't be found.");
            return false;
        }
        
        /// <summary> Returns empty AudioSource, Null if all Sources are used </summary>
        private TWrapper GetAudioSource() {
            
            // Find a free Source and return it
            TWrapper foundAudio = _sources.FirstOrDefault(wrapper => !wrapper.IsPlaying);
            if (foundAudio != default) return foundAudio;
            
            // If we didnt find the SoundSource, we need to free the source with the lovest volume
            // note that this could be prevented by using one more wrapper, but its too rare that it matters
            TWrapper lowestWrapper = _sources.OrderBy(source => source.Volume).First();
            lowestWrapper.FreeAudioSource();
            return lowestWrapper;
        }
        
        /// <summary> Stops the currently playing Track </summary>
        public void StopMusic() {
            foreach (TWrapper source in _sources) {
                if(source.IsPlaying) source.FadeOut();
            }
        }
        
    }
    
}