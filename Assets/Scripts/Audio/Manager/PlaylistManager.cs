using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Beta.Audio;
using UnityEngine;

namespace Beta.Audio {

    /// <summary> Playlist Function, requires MusicManager </summary>
    /// <remarks> Written by Nikolas </remarks>
    [RequireComponent(typeof(MusicManager))]
    public class PlaylistManager : MonoBehaviour {

        [Tooltip("If the music starts playing automatically at the beginning")] [SerializeField]
        private bool _autoplay = true;

        [Tooltip("If true music is randomly ordered")] [SerializeField]
        private bool _randomShuffle = true;

        /// <summary> Reference to the PlaylistCoroutine </summary>
        private Coroutine _playRoutine;

        /// <summary> A Ref to the MusicManager </summary>
        private MusicManager _manager;

        /// <summary> A random number generator for the playlist shuffle</summary>
        private System.Random _random = new System.Random();

        private bool _continuePlaylist;

        /// <summary> Moves GameObject to DontDestroyOnLoadScene </summary>
        private void Start() {

            _manager = GetComponent<MusicManager>();

            if (_autoplay) StartPlaylist();
            
            DontDestroyOnLoad(gameObject);
        }

        /// <summary> Starts the playlist </summary>
        public void StartPlaylist() {
            if (_playRoutine != null) return;
            _playRoutine = StartCoroutine(Playlist());
        }

        /// <summary> Stops the Playlist from playing more Events </summary>
        /// <param name="force"> If the Playlist is stopped immediately or only after the song ends </param>
        public void EndPlaylist(bool force = false) {
            if (force) {
                _manager.StopMusic();
                StopCoroutine(_playRoutine);
            }
            else _continuePlaylist = false;
        }

        /// <summary> Loops the Playlist </summary>
        private IEnumerator Playlist() {

            AudioEvent[] availableEvents = _manager.AudioEvents;
            float fadeTime = _manager.FadeTime;
            _continuePlaylist = true;
            
            while (_continuePlaylist) {
                if (_randomShuffle) availableEvents = availableEvents.OrderBy(a => _random.Next()).ToArray();

                foreach (AudioEvent audioEvent in availableEvents) {
                    if (!_manager.PlayMusic(audioEvent.AudioName)) yield return null;
                    else {
                        //If this value is negative, the coroutine will break
                        yield return new WaitForSeconds(audioEvent.Length - fadeTime);
                    }

                }
            }
        }
    }
}
