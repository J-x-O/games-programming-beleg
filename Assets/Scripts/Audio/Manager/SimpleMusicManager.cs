using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Beta.Audio;
using UnityEngine;
using UnityEngine.InputSystem.Utilities;

namespace Beta.Audio {

    /// <summary> Manages non-adaptive Music </summary>
    /// <remarks> Written by Nikolas </remarks>
    public class SimpleMusicManager : MusicManager<SimpleMusicAudioEvent, SimpleSoundSourceWrapper> {

        protected override SimpleSoundSourceWrapper SetupWrapper() {
            AudioSource source = gameObject.AddComponent<AudioSource>();
            return new SimpleSoundSourceWrapper(source, _fadeTime);
        }
    }
}
