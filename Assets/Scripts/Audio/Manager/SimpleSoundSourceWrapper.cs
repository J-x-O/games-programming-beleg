using System;
using System.Collections;
using System.Collections.Generic;
using Beta.Audio;
using UnityEngine;
using UnityEngine.InputSystem;

public class SimpleSoundSourceWrapper : SoundSourceWrapper<SimpleMusicAudioEvent> {

    /// <summary> If the <see cref="AudioSource"/> is currently Playing </summary>
    public override bool IsPlaying => _source.isPlaying;

    /// <summary> The current Volume of the <see cref="AudioSource"/> </summary>
    public override float Volume => _source.volume;
    
    /// <summary> The <see cref="AudioSource"/>, that is being managed by this wrapper </summary>
    private AudioSource _source;

    /// <summary> Default Constructor </summary>
    /// <param name="source"> The <see cref="AudioSource"/>, that is being managed by this wrapper </param>
    /// <param name="fadeDuration"> How long a fade takes, until its done </param>
    public SimpleSoundSourceWrapper(AudioSource source, float fadeDuration) {
        _source = source;
        _fadeDuration = fadeDuration;
    }

    /// <summary> A Method for Playing and Fading in a MusicEvent </summary>
    /// <param name="musicEvent"> The Music Event that will be played</param>
    public override void Play(SimpleMusicAudioEvent musicEvent) {
        _source.clip = musicEvent.Clip;
        _source.pitch = 1f;
        _source.Play();
        FadeIn(musicEvent.Volume);
    }

    /// <summary> A method to increase the volume of wrapped <see cref="AudioSource"/> </summary>
    /// <param name="targetVolume"> The value the Volume will be increased to over time </param>
    private void FadeIn(float targetVolume) {
        
        // Calculate Initial Variables
        float startVolume = Volume;
        float startProgress = Mathf.Clamp01(startVolume / targetVolume);
        _currentTargetVolume = targetVolume;
        
        // Start a new LeanTween to increase the volume over time
        if(_fadeTween != null) LeanTween.cancel(_fadeTween.uniqueId);
        _fadeTween = LeanTween.value(startVolume, targetVolume, _fadeDuration * startProgress)
            .setEaseInSine()
            .setOnUpdate(progress => _source.volume = progress);
    }

    public override void FadeOut() => FadeOut(null);
    
    /// <summary> A Method that fades out and stops the wrapped <see cref="AudioSource"/> </summary>
    /// <param name="onFadeEnd"> An action that will be executed as soon as the Fade is over </param>
    public void FadeOut(Action onFadeEnd) {
        
        // Calculate Initial Variables
        float startVolume = _source.volume;
        if (_source.volume == 0 || _currentTargetVolume == 0) return;
        float startProgress = Mathf.Clamp01(startVolume / _currentTargetVolume);
        
        // Start the new LeanTween to turn down the Audio over Time
        if(_fadeTween != null) LeanTween.cancel(_fadeTween.uniqueId);
        _fadeTween = LeanTween.value(startVolume, 0, _fadeDuration * startProgress)
            .setEaseInSine()
            .setOnUpdate(progress => _source.volume = progress)
            .setOnComplete(FreeAudioSource)
            .setOnComplete(() => onFadeEnd?.Invoke());
    }

    /// <summary> Method to forcefully reset the </summary>
    public override void FreeAudioSource() {
        if(_fadeTween != null) LeanTween.cancel(_fadeTween.uniqueId);
        _source.Stop();
        _source.volume = 0;
        _currentTargetVolume = 0;
    }
}
