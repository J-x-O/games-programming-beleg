﻿using UnityEngine;

namespace Beta.Audio {
    
    public abstract class SoundSourceWrapper<TAudioEvent> where TAudioEvent : AudioEvent {

        public abstract bool IsPlaying { get; }
        public abstract float Volume { get; }
        
        /// <summary> How long a fade takes, until its done </summary>
        protected float _fadeDuration;

        /// <summary> A ref to the LeanTween, that is controlling the fade </summary>
        protected LTDescr _fadeTween;
    
        /// <summary> The targetVolume we are fading towards </summary>
        protected float _currentTargetVolume;

        public abstract void Play(TAudioEvent audioEvent);
        
        public abstract void FadeOut();
        public abstract void FreeAudioSource();

    }
}