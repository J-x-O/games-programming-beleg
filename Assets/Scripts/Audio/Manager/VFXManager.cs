using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Beta.Audio {

    /// <summary>Manages events</summary>
    /// <author>Nikolas</author>
    public class VFXManager : MonoBehaviour {

        /// <summary>Used to add events</summary>
        [SerializeField] [Tooltip("Used to add event")]
        private VFXAudioEvent[] _vfxEvents;

        /// <summary>Stores Events</summary>
        private Dictionary<string, VFXAudioEvent> _dictionary = new Dictionary<string, VFXAudioEvent>();

        /// <summary>Reference to AudioSource</summary>
        private AudioSource _source;

        /// <summary>Turns the array into dictionary and creates AudioSource</summary>
        /// <remarks>Nikolas</remarks>
        void Awake() {
            foreach (VFXAudioEvent s in _vfxEvents) {
                _dictionary.Add(s.name, s);
            }

            _source = gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        }

        /// <summary>Plays the event</summary>
        /// <param name="name">Name of the event, which shall be played</param>
        /// <remarks>Nikolas</remarks>
        public bool PlayVFX(string name) {

            if (_dictionary.ContainsKey(name)) {
                _dictionary[name].Play(_source);
                return true;
            }
            else {
                Debug.LogWarning("VFX " + name + " kann nicht gefunden werden");
                return false;
            }
        }
    }
}
