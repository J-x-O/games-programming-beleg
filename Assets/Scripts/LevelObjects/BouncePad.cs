using System;
using UnityEngine;

namespace LevelObjects {

    public class BouncePad : MonoBehaviour {

        [Tooltip("Multiplier for the current overall Velocity")]
        [SerializeField] private float _overallMultiplier = 3;
        
        [Tooltip("Multiplier for the current vertical Velocity, which will be redircted")]
        [SerializeField] private float _bounceStrength = 10;

        private void OnCollisionEnter(Collision other) {

            Rigidbody otherRigidbody = other.rigidbody;
            if (otherRigidbody == null) return;
            
            Vector3 velocity = otherRigidbody.velocity;
            velocity *= _overallMultiplier;
            velocity.y = Mathf.Abs(velocity.y) * _bounceStrength;
            otherRigidbody.velocity = velocity;
        }
    }

}