using System;
using UnityEngine;

namespace LevelObjects {

    public class Checkpoint : MonoBehaviour {

        /// <summary> The currently active CheckPoint</summary>
        public static Checkpoint CurrentCheckpoint { get; private set; }

        [Tooltip("If true, this checkpoint will be set as the active checkpoint at the start")]
        [SerializeField] private bool _initialSpawnPoint;
        
        /// <summary> The Position where the Player will spawn </summary>
        public Transform SpawnPosition => _spawnPosition;
        
        [Tooltip("The Position where the Player will spawn")]
        [SerializeField] private Transform _spawnPosition;
        
        private int _playerLayer;

        private void Awake() {
            _playerLayer = LayerMask.NameToLayer("Player");
            
            if (!_initialSpawnPoint) return;
            SetCurrentCheckpointToThis();
        }

        private void OnTriggerEnter(Collider other) {
            
            // Check if the collided object is the player
            if (other.gameObject.layer != _playerLayer) return;
            SetCurrentCheckpointToThis();
        }

        private void SetCurrentCheckpointToThis() {
            
            // Check if we have a spawn Position
            if (_spawnPosition == null) {
                Debug.LogWarning("We hit a Checkpoint, but there was no spawn position, there for it was ignored");
                return;
            }
            
            // if everything is fine, update the Checkpoint
            CurrentCheckpoint = this;
        }
        
    }

}