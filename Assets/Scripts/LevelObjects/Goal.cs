using System;
using UnityEngine;

namespace LevelObjects {

    public class Goal : MonoBehaviour {

        public static event Action OnLevelFinished;

        private bool _eventInvoked;
        
        private int _playerLayer;

        private void Awake() => _playerLayer = LayerMask.NameToLayer("Player");

        private void OnTriggerEnter(Collider other) {

            // Check if the object is the player
            if (other.gameObject.layer != _playerLayer) return;

            if (_eventInvoked) return;
            OnLevelFinished?.Invoke();
            _eventInvoked = true;
        }
    }
}