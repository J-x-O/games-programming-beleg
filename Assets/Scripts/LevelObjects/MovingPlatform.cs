using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace LevelObjects {

    [RequireComponent(typeof(Rigidbody))]
    public class MovingPlatform : MonoBehaviour {

        private enum IterationMode { PingPong, Circle }
        
        private enum SpeedMode { Distance, FromOneToNext }
        
        [Tooltip("How the list of Transforms will be traversed")]
        [SerializeField] private IterationMode _iterationMode;
        
        [Tooltip("All Transforms making up the path of the Platform")]
        [SerializeField] private List<Transform> _path = new List<Transform>();

        [Tooltip("Dictates how the Speed will be Prcessed, Distance means ")]
        [SerializeField] private SpeedMode _speedMode;
        
        [SerializeField] private float _speed;

        private Rigidbody _rigidbody;

        private void Awake() {
            _rigidbody = GetComponent<Rigidbody>();
        }

        private IEnumerator Start() {

            // Prepare the Path
            List<Transform> preparedPath = AggregatePath();
            
            while (true) {

                // Loop over the Path
                foreach (Transform target in preparedPath) {
                    Vector3 startPos = transform.position;
                    float progress = 0;

                    // Move until we hit the exit condition
                    bool arrived = false;
                    while (!arrived) {
                        
                        Vector3 targetPos;
                        switch (_speedMode) {
                            
                            // Move the Object towards the Target
                            // If the Value wasn't clamped, we moved exactly to the position, since we are in range
                            case SpeedMode.Distance:
                                Vector3 position = transform.position;
                                float distance = _speed * Time.deltaTime;
                                
                                Vector3 direction = Vector3.ClampMagnitude(target.position - position, distance);
                                targetPos = position + direction;
                                
                                arrived = direction.magnitude < distance + 0.01f;
                                break;

                            // Lerp the object over the time period towards the target
                            // If the Progress is bigger than 1 we arrived at the Target
                            case SpeedMode.FromOneToNext:
                                targetPos = Vector3.Lerp(startPos, target.position, progress / _speed);
                                arrived = progress > 1f; 
                                progress += Time.deltaTime;
                                break;

                            // shouldn't happen, exit out
                            default: yield break;
                        }
                        _rigidbody.MovePosition(targetPos);
                        yield return null;
                    }
                }
            }
        }

        private List<Transform> AggregatePath() {
            
            if (_path == null) return null;
            
            List<Transform> _preparedList;
            switch (_iterationMode) {
                case IterationMode.PingPong:

                    // Discard the first item, since we will already be at that position
                    _preparedList = _path
                        .Where((item, index) => index != 0).ToList();

                    // Invert the List (Enumerable to avoid the InPlace override provided by List<>)
                    List<Transform> reversed = _path.AsEnumerable()
                        .Reverse()
                        .Where((item, index) => index != 0).ToList();
                    _preparedList.AddRange(reversed);
                    return _preparedList;

                case IterationMode.Circle:

                    // Discard the first item, since we will already be at that position
                    _preparedList = _path
                        .Where((item, index) => index != 0).ToList();

                    // Add the ciscarded Item at the end to close the circle
                    _preparedList.Add(_path.First());
                    return _preparedList;

                default: return null;
            }
        }
    }
}