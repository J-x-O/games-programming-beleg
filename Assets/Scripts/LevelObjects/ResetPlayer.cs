using System;
using GameProgramming.Movement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LevelObjects {

    public class ResetPlayer : MonoBehaviour {
        
        private int _playerLayer;

        private void Awake() => _playerLayer = LayerMask.NameToLayer("Player");

        private void OnTriggerEnter(Collider other) {
            
            // Check if the object is the player
            if (other.gameObject.layer != _playerLayer) return;

            // Check if we have a checkpoint to go back to
            if (Checkpoint.CurrentCheckpoint == null) {
                Debug.LogError("Critical Failure, there was no Checkpoint to reset the Player to");
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                return;
            }
            
            // Check if we have a MoveSet script
            if (!other.TryGetComponent(out MoveSet _moveSet)) return;
            
            // Reset the Velocity of the rigidbody, reset the player position,
            // remove any additional speed we gained while falling
            _moveSet.Rigidbody.velocity = Vector3.zero;
            _moveSet.transform.position = Checkpoint.CurrentCheckpoint.SpawnPosition.position;
            _moveSet.MovementSpeedManager.SetAdditionalSpeed(0f);
        }
    }
}