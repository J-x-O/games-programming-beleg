using System;
using UnityEngine;
using UnityEngine.VFX;

namespace LevelObjects.Visuals {

    [RequireComponent(typeof(VisualEffect))]
    public class TriggerConfetti : MonoBehaviour {
        
        private VisualEffect _effect;
        
        private void Awake() {
            _effect = GetComponent<VisualEffect>();
            _effect.Stop();
        }

        private void OnEnable() => Goal.OnLevelFinished += _effect.Play;

        private void OnDisable() => Goal.OnLevelFinished -= _effect.Play;
    }

}