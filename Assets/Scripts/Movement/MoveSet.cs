using System;
using System.Collections.Generic;
using System.Linq;
using GameProgramming.Utility.TypeBasedEventSystem;
using UnityEngine;

namespace GameProgramming.Movement {

    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(GroundCheck))]
    [RequireComponent(typeof(MovementSpeedManager))]
    public class MoveSet : MonoBehaviour {
        
        // Active References to other Components
        
        /// <summary> A Ref to the <see cref="Rigidbody"/> of this Object </summary>
        public Rigidbody Rigidbody { get; private set; }
        
        /// <summary> A Ref to the <see cref="GroundCheck"/> of this Object </summary>
        public GroundCheck GroundCheck { get; private set; }

        /// <summary> The <see cref="Movement.MovementSpeedManager"/> of this Object </summary>
        public MovementSpeedManager MovementSpeedManager { get; private set; }


        // Movement State Management
        
        /// <summary> An event system handling the different MovementStates </summary>
        public TypeBasedEventSystem<MovementState> EventSystem { get; private set; }

        /// <summary> The Current MovementState of this Object </summary>
        public MovementState CurrentMovementState { get; private set; }
        
        [SerializeReferenceButton] [SerializeReference] [SerializeField]
        private List<MovementState> _movementStates;

        /// <summary> A Map of the MovementStates and their Types, to avoid searching </summary>
        private Dictionary<Type, MovementState> _dictionary;
        
        
        // Current Directions
        
        /// <summary> The direction this unit is moving towards, used to control this unit </summary>
        /// <remarks> This will be computed relative to the currentLookDirection </remarks>
        public Vector2 CurrentMoveDirection {
            get => _currentMoveDirection;
            set => _currentMoveDirection = Vector2.ClampMagnitude(value, 1);
        }
        private Vector2 _currentMoveDirection = Vector2.zero;
        
        /// <summary> The direction this unit is looking / facing, used to control this unit </summary>
        public Vector2 CurrentLookDirection {
            get => _currentLookDirection;
            set => _currentLookDirection = Vector2.ClampMagnitude(value, 1);
        }
        private Vector2 _currentLookDirection = Vector2.zero;

        
        // Methods
        
        private void Awake() {
            Rigidbody = GetComponent<Rigidbody>();
            GroundCheck = GetComponent<GroundCheck>();
            MovementSpeedManager = GetComponent<MovementSpeedManager>();
            EventSystem = new TypeBasedEventSystem<MovementState>();

            _dictionary = new Dictionary<Type, MovementState>();
            foreach (MovementState state in _movementStates) {
                _dictionary[state.GetType()] = state;
            }
        }
        
        private void OnEnable()
            => _movementStates.ForEach(state => state.Initialize(this));

        private void OnDisable()
            => _movementStates.ForEach(state => state.Dispose(this));

        private void FixedUpdate() {
            CurrentMovementState?.HandleMovement(this, _currentMoveDirection, _currentLookDirection);
        }

        /// <summary> Activate a certain Movement State </summary>
        /// <typeparam name="T"> The Type of the Movement State </typeparam>
        public bool SetMovementState<T>() where T : MovementState {

            // Try Getting the MovementState
            T state = GetMovementState<T>();
            if (state == null) return false;
            
            // Test if the State Can be activated
            if (!state.CanBeActivated(this)) return false;

            // Clean Up the old MovementState, if it exists
            if (CurrentMovementState != null) {
                CurrentMovementState.CleanUp(this);
                EventSystem.InvokeTypeBasedEventEnd(CurrentMovementState);
            }

            // Start the new MovementState
            CurrentMovementState = state;
            CurrentMovementState.Setup(this);
            EventSystem.InvokeTypeBasedEventStart(state);
            return true;
        }

        /// <summary> Retrieves a certain Movement State </summary>
        /// <typeparam name="T">  The Type of the Movement State </typeparam>
        /// <returns> The found Movement State, null if no one was found </returns>
        public T GetMovementState<T>() where T : MovementState {
            
            // Try getting the MovementState
            if (_dictionary.TryGetValue(typeof(T), out MovementState state)) return state as T;
            
            Debug.LogError($"MovementState {typeof(T)} Couldn't be Found");
            return null;

        }
    }
}