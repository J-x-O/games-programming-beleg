using UnityEngine;

namespace GameProgramming.Movement {
    
    [CreateAssetMenu(menuName = "GamesProgramming Beleg/Movement Speed")]
    public class MovementSpeedAsset : ScriptableObject {

        /// <summary> The BaseSpeed this Object has when no state is active (shouldn't happen) </summary>
        public float BaseSpeed => _baseSpeed;

        /// <summary> How fast the speed increases to a higher value </summary>
        public float ClimbRate => _climbRate;
        
        /// <summary> How fast the speed decreases to a lower value </summary>
        public float DeclineRate => _declineRate;
        
        /// <summary> What percentage of the target speed will trigger an increase </summary>
        public float AdditionalSpeedThreshold => _additionalSpeedThreshold;

        /// <summary> How fast the additional Speed increases, if the threshold is met </summary>
        public float AdditionalSpeedClimbRate => _additionalSpeedClimbRate;
        
        /// <summary> How fast the additional Speed decreases, if the threshold isn't met </summary>
        public float AdditionalSpeedDeclineRate => _additionalSpeedDeclineRate;
        
        [Tooltip("The BaseSpeed this Object has when no state is active (shouldn't happen)")]
        [SerializeField] private float _baseSpeed;

        [Tooltip("How fast the speed increases to a higher value")]
        [SerializeField] private float _climbRate;
        
        [Tooltip("How fast the speed decreases to a lower value")]
        [SerializeField] private float _declineRate;
        
        [Header("Additional Speed")]
        [Tooltip("What percentage of the target speed will trigger an increase")]
        [Range(0,1)]
        [SerializeField] private float _additionalSpeedThreshold;

        [Tooltip("How fast the additional Speed increases, if the threshold is met")]
        [SerializeField] private float _additionalSpeedClimbRate;
        
        [Tooltip("How fast the additional Speed decreases, if the threshold isn't met")]
        [SerializeField] private float _additionalSpeedDeclineRate;
    }
}