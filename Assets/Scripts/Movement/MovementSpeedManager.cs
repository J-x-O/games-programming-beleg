using System;
using UnityEngine;
using Utility;

namespace GameProgramming.Movement {

    public struct MovementUpdateData {
        public float CurrentMaxSpeed;
        public float TargetMaxSpeed;
        public float CurrentAdditionalSpeed;
        public float CurrentSpeed;
    }
    
    /// <summary> A ManagerClass that handles the Speed of this Unit </summary>
    public class MovementSpeedManager : MonoBehaviour {

        public event Action<MovementUpdateData> OnMovementDataChanged;
        
        /// <summary> The maximum legal Speed this Unit can currently Move </summary>
        public float SpeedCap => CurrentMaxSpeed + CurrentAdditionalSpeed;

        /// <summary> The true Speed this Unit is currently moving at </summary>
        public float CurrentSpeed {
            get {
                if (_rigidbody == null) return 0;
                return _rigidbody.velocity.xz().magnitude;
            }
        }
        
        [Tooltip("The Asset containing all Values")]
        [SerializeField] private MovementSpeedAsset _asset;

        /// <summary> the maximum legal speed without the additional Speed </summary>
        public float CurrentMaxSpeed { get; private set; }

        /// <summary> The maximum legal Speed Target, the <see cref="CurrentMaxSpeed"/> will move towards </summary>
        private float _targetMaxSpeed;

        /// <summary> The Additional Speed this Unit can build up over time </summary>
        public float CurrentAdditionalSpeed { get; private set; }

        private Rigidbody _rigidbody;

        private void Awake() {
            _rigidbody = GetComponent<Rigidbody>();
            _targetMaxSpeed = _asset.BaseSpeed;
            CurrentMaxSpeed = _targetMaxSpeed;
        }

        private void Update() {

            // if the targetMaxSpeed is greater than our currentMaxSpeed, we need to increase
            if (CurrentMaxSpeed < _targetMaxSpeed) {
                CurrentMaxSpeed += _asset.ClimbRate * Time.deltaTime;
                CurrentMaxSpeed = Mathf.Min(_targetMaxSpeed, CurrentMaxSpeed);
            }

            // if the targetMaxSpeed is smaller than our currentMaxSpeed, we need to decrease
            if (CurrentMaxSpeed > _targetMaxSpeed) {
                CurrentMaxSpeed -= _asset.DeclineRate * Time.deltaTime;
                CurrentMaxSpeed = Mathf.Max(0, CurrentMaxSpeed);
            }
            
            // If we come close to the combined Maximum, we want to raise the additional speed
            if (CurrentSpeed > SpeedCap * _asset.AdditionalSpeedThreshold) {
                CurrentAdditionalSpeed += _asset.AdditionalSpeedClimbRate * Time.deltaTime;
            }
            else {
                CurrentAdditionalSpeed -= _asset.AdditionalSpeedDeclineRate * Time.deltaTime;
                CurrentAdditionalSpeed = Math.Max(0, CurrentAdditionalSpeed);
            }
            
            // Invoke Event with new Data;
            OnMovementDataChanged?.Invoke(new MovementUpdateData() {
                CurrentMaxSpeed = CurrentMaxSpeed,
                TargetMaxSpeed = _targetMaxSpeed,
                CurrentAdditionalSpeed = CurrentAdditionalSpeed,
                CurrentSpeed = CurrentSpeed
            });
        }
        
        /// <summary> Sets the current TargetMaxSpeed, causing the <see cref="SpeedCap"/> to gradually increase </summary>
        public void SetMaxSpeed(float value) => _targetMaxSpeed = value;

        /// <summary> Sets the AdditionalSpeed, which the player builds over time </summary>
        public void SetAdditionalSpeed(float value) => CurrentAdditionalSpeed = value;
    }
}