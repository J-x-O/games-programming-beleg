using System;
using UnityEngine;

namespace GameProgramming.Movement {

    /// <summary> A Class that defines how an Object moves </summary>
    [Serializable]
    public abstract class MovementState {

        /// <summary> Initialize this State, to manage Preparation </summary>
        /// <param name="moveSet"> The Target for the Movement </param>
        public virtual void Initialize(MoveSet moveSet) { }

        /// <summary> Remove this State, to manage CleanUp </summary>
        /// <param name="moveSet"> The Target for the Movement </param>
        public virtual void Dispose(MoveSet moveSet) { }

        /// <summary> If the Activation is Legal </summary>
        /// <param name="moveSet"> The Target for the Movement </param>
        public virtual bool CanBeActivated(MoveSet moveSet) { return true; }
        
        /// <summary> Prepare the Activation of this MovementState </summary>
        /// <param name="moveSet"> The Target for the Movement </param>
        public virtual void Setup(MoveSet moveSet)  { }

        /// <summary> AfterCare when Deactivating this MovementState </summary>
        /// <param name="moveSet"> The Target for the Movement </param>
        public virtual void CleanUp(MoveSet moveSet) { }

        /// <summary> Defines how this Unit Actually Moves </summary>
        /// <param name="moveSet"> The Target for the Movement </param>
        /// <param name="moveDirection"> The direction this Unit should move in </param>
        /// <param name="lookDirection"> The direction this Unit is facing </param>
        public virtual void HandleMovement(MoveSet moveSet, Vector2 moveDirection, Vector2 lookDirection) { }


    }
}