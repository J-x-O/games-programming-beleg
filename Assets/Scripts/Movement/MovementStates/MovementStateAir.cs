using System;

namespace GameProgramming.Movement {

    [Serializable]
    public class MovementStateAir : MovementStateGround {

        public override void Initialize(MoveSet moveSet) {
            _target = moveSet;
            moveSet.GroundCheck.OnGrounded += HandleOnGrounded;
        } 

        public override void Dispose(MoveSet moveSet)
            => moveSet.GroundCheck.OnGrounded -= HandleOnGrounded;

        private void HandleOnGrounded() {
            if(_target.CurrentMovementState == this)
                _target.SetMovementState<MovementStateGround>();
        }

        public override bool CanBeActivated(MoveSet moveSet) {
            return !moveSet.GroundCheck.Grounded;
        }
        
    }

}