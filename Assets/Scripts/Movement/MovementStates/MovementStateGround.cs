using System;
using UnityEngine;
using Utility;

namespace GameProgramming.Movement {

    [Serializable]
    public class MovementStateGround : MovementState {

        [Tooltip("How fast we can ChangeDirections")] [Range(0,1)]
        [SerializeField] private float _lerpPercentage;

        [Tooltip("How fast we can Move")]
        [SerializeField] private float _speed;

        protected MoveSet _target;

        public override void Initialize(MoveSet moveSet) {
            _target = moveSet;
            moveSet.GroundCheck.OnUngrounded += HandleOnUngrounded;
        }

        public override void Dispose(MoveSet moveSet)
            => moveSet.GroundCheck.OnUngrounded -= HandleOnUngrounded;
        
        private void HandleOnUngrounded() {
            if(_target.CurrentMovementState == this)
                _target.SetMovementState<MovementStateAir>();
        }

        public override bool CanBeActivated(MoveSet moveSet) =>  moveSet.GroundCheck.Grounded;

        public override void Setup(MoveSet moveSet) => moveSet.MovementSpeedManager.SetMaxSpeed(_speed);

        /// <summary> Handle the Basic Walking Movement </summary>
        /// <param name="moveSet"> The Target for the Movement </param>
        /// <param name="moveDirection"> The direction this Unit should move in </param>
        /// <param name="lookDirection"> The direction this Unit is facing </param>
        public override void HandleMovement(MoveSet moveSet, Vector2 moveDirection, Vector2 lookDirection) {

            // Cache Rigidbody and Velocity
            Rigidbody rigidBody = moveSet.Rigidbody;
            Vector3 currentVelocity = rigidBody.velocity;
            
            // Calculate TargetVelocity (relative towards look direktion)
            Vector2 relativeDirection = moveDirection.RelativeCombine(lookDirection);
            Vector3 clampedDirection = Vector3.ClampMagnitude(relativeDirection.xOy(), 1);
            Vector3 targetVelocity =  clampedDirection * moveSet.MovementSpeedManager.SpeedCap;
            
            // Carry over current y velocity
            targetVelocity.y = currentVelocity.y;
            
            // Lerp to the new desired Direction
            Vector3 lerpDirection = Vector3.Lerp(currentVelocity, targetVelocity, _lerpPercentage);
            rigidBody.velocity = lerpDirection;

            // Update the Player Looking Direction
            lerpDirection.y = 0;
            if (lerpDirection.magnitude < 0.01f) return;
            moveSet.transform.forward = lerpDirection;
        }
    }
}