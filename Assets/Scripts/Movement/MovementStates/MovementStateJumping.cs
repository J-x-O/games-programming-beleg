using System;
using GameProgramming.Player.Input;
using GameProgramming.Utility.TypeBasedEventSystem;
using UnityEngine;

namespace GameProgramming.Movement {

    [Serializable]
    public class MovementStateJumping : MovementState, ITypeBasedEventHandler<MovementState> {

        public event Action<int> OnJumpCountChanged;
        
        [Tooltip("How often the User Can Jump")]
        [SerializeField] private int _jumpCount;

        [Tooltip("How high the User will Jump")]
        [SerializeField] private float _jumpHeight;

        [Tooltip("How much the XZ-Velocity will be boosted")]
        [SerializeField] private float _movementBoost;

        [Tooltip("How long the Jump (the boost phase) lasts for")]
        [SerializeField] private float _duration;

        /// <summary> How many Jumps are currently left </summary>
        private int _jumpsLeft {
            set {
                OnJumpCountChanged?.Invoke(value);
                _internalJumpsLeft = value;
            }
            get => _internalJumpsLeft;
        }
        /// <summary> The internal Jump Counter, use _jumpsLeft instead </summary>
        private int _internalJumpsLeft;

        /// <summary> LeanTween executing the current Jump </summary>
        private LTDescr _currentJump;


        public override void Initialize(MoveSet moveSet) {

            // Handle Subscribing
            moveSet.GroundCheck.OnGrounded += ResetJumpCounter;
            moveSet.EventSystem.RegisterHandler(this, typeof(MovementStateWallRun));
        } 

        public override void Dispose(MoveSet moveSet) {
            
            // Handle Unsubscribing
            moveSet.GroundCheck.OnGrounded -= ResetJumpCounter;
            moveSet.EventSystem.UnregisterHandler(this, typeof(MovementStateWallRun));
            
            // Cancel LeanTween for Jump, if needed
            if(_currentJump != null) LeanTween.cancel(_currentJump.uniqueId);
            _currentJump = null;
        }

        /// <summary> Handle Wall Run Started </summary>
        /// <remarks> this should only be called if we successfully stuck to a wall, we dont need to check the state </remarks>
        /// <param name="instanceOfType"> the active instance, passed in by the event system </param>
        public void HandleTypeBasedEventStart(MovementState instanceOfType) {
            MovementStateWallRun.State state = ((MovementStateWallRun) instanceOfType).CurrentState;
            if (state != MovementStateWallRun.State.Failed) ResetJumpCounter();
        }

        /// <summary> Handle Wall Run Ended, doesn't need to do anything in this context</summary>
        public void HandleTypeBasedEventEnd(MovementState instanceOfType) { }

        private void ResetJumpCounter() {
            _jumpsLeft = _jumpCount;
        }


        public override bool CanBeActivated(MoveSet moveSet) => _jumpsLeft > 0 && _currentJump == null;

        /// <summary> Executes the Jump, as soon as the Movement is activated </summary>
        /// <param name="moveSet"> The Target for the Movement </param>
        public override void Setup(MoveSet moveSet) {
            
            // Update the JumpsLeftCounter
            _jumpsLeft--;

            // Calculate the target Velocity
            Vector3 velocity = moveSet.Rigidbody.velocity;
            Vector3 targetVelocity = velocity * _movementBoost;
            targetVelocity.y = Mathf.Max(0, targetVelocity.y);
            targetVelocity.y += _jumpHeight;

            // Tween towards the desired Velocity, Exit this Movement State
            _currentJump = LeanTween.value(moveSet.gameObject, velocity, targetVelocity, _duration)
                .setEaseInSine()
                .setOnUpdateVector3(velo => moveSet.Rigidbody.velocity = velo)
                .setOnComplete(() => {
                    moveSet.SetMovementState<MovementStateAir>();
                    moveSet.SetMovementState<MovementStateGround>();
                });
        }

        /// <summary> Cancel the Old LeanTween, if necessary </summary>
        /// <param name="moveSet"> The Target for the Movement </param>
        public override void CleanUp(MoveSet moveSet) {
            if(_currentJump != null) LeanTween.cancel(_currentJump.uniqueId);
            _currentJump = null;
        }
    }
}