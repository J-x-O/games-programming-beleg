using System;
using UnityEngine;
using Utility;

namespace GameProgramming.Movement {

    [Serializable]
    public class MovementStateSliding : MovementState {

        [Tooltip("The Physic Material that will be used when sliding")]
        [SerializeField] private PhysicMaterial _slideMaterial;

        [Tooltip("The maximum angle that can be turned in one second")]
        [SerializeField] private float _maxInputAngle = 5;
        
        [Tooltip("The constant force being applied downwards")]
        [SerializeField] private float _downForce = 5;

        private float _initialVelocity;
        
        private Collider _collider;

        private PhysicMaterial _startMaterial;
        
        public override void Initialize(MoveSet moveSet) {
            _collider = moveSet.GetComponent<Collider>();
        }

        public override void Setup(MoveSet moveSet) {
            _startMaterial = _collider.material;
            _collider.material = _slideMaterial;
            _initialVelocity = moveSet.MovementSpeedManager.CurrentSpeed;
            moveSet.MovementSpeedManager.SetMaxSpeed(0);
        }

        public override void CleanUp(MoveSet moveSet) {
            _collider.material = _startMaterial;

            // Carry over generated Speed
            MovementSpeedManager manager = moveSet.MovementSpeedManager;
            float currentSpeed = manager.CurrentSpeed - _initialVelocity;
            if(currentSpeed > manager.CurrentAdditionalSpeed) manager.SetAdditionalSpeed(currentSpeed);
        }

        public override void HandleMovement(MoveSet moveSet, Vector2 moveDirection, Vector2 lookDirection) {

            // Cache the important directions
            Vector3 targetDirection = moveDirection.RelativeCombine(lookDirection).xOy();
            Vector3 currentDirection = moveSet.Rigidbody.velocity;
            
            // Get and Limit the Angle between the Target and the current Direction
            float angle = Vector3.SignedAngle(currentDirection, targetDirection, Vector3.up);
            angle = Mathf.Clamp(angle, -_maxInputAngle * Time.fixedDeltaTime, _maxInputAngle * Time.fixedDeltaTime);

            // Rotate Direction, carry over UpDown Movement, add the downForce
            Vector3 newDirection = Quaternion.AngleAxis(angle, new Vector3(0, 1, 0)) * currentDirection;
            newDirection.y = currentDirection.y;
            newDirection.y -= _downForce * Time.deltaTime;
            
            // Apply the modified Direction to the Rigidbody
            moveSet.Rigidbody.velocity = newDirection;

            // Update the Player Looking Direction
            newDirection.y = 0;
            if (newDirection.magnitude < 0.01f) return;
            moveSet.transform.forward = newDirection;
        }
    }

}