using System;
using UnityEngine;
using Utility;

namespace GameProgramming.Movement {

    [Serializable]
    public class MovementStateWallRun : MovementState {

        public enum State { WallLeft, WallRight, Failed }
        
        public State CurrentState { get; private set; }

        [Tooltip("How fast we can Move")]
        [SerializeField] private float _speed = 20;
        
        [Tooltip("How strong we will jump off the wall in the end")]
        [SerializeField] private float _wallBoostStrength = 10;

        [Tooltip("How near a wall has to be to be registered")]
        [SerializeField] private float _wallDetectionDistance = 2;
        
        [Tooltip("How far the player will be when wall running")]
        [SerializeField] private float _wallRunDistance = 1;
        
        [Tooltip("How fast we get glued onto the wall")] [Range(0,1)]
        [SerializeField] private float _lerpPercentage = 0.01f;
        
        private LayerMask _wallLayer;

        public override void Initialize(MoveSet moveSet) {
            _wallLayer = LayerMask.GetMask("Wall");
        }

        public override bool CanBeActivated(MoveSet moveSet) {

            // Only allow wall runs if you jumped onto the wall
            if (moveSet.GroundCheck.Grounded) return false;
            
            // Do a WallCheck
            CurrentState = WallTest(moveSet, out RaycastHit hit);
            
            // return if it worked
            return CurrentState != State.Failed;
        }

        public override void Setup(MoveSet moveSet) {
            moveSet.Rigidbody.useGravity = false;
            moveSet.MovementSpeedManager.SetMaxSpeed(_speed);
        }

        public override void CleanUp(MoveSet moveSet) {

            moveSet.Rigidbody.useGravity = true;

            // Boost Player from the Wall
            Vector3 right = moveSet.transform.right;
            CurrentState = WallTest(moveSet, out RaycastHit hit);
            switch (CurrentState) {
                case State.WallRight : moveSet.Rigidbody.velocity += - right * _wallBoostStrength; break;
                case State.WallLeft : moveSet.Rigidbody.velocity += right * _wallBoostStrength; break;
            }
        }

        public override void HandleMovement(MoveSet moveSet, Vector2 moveDirection, Vector2 lookDirection) {
            
            // Check if we are still on a Wall
            CurrentState = WallTest(moveSet, out RaycastHit hit);
            bool failed = CurrentState == State.Failed;
            
            // Check if we are steering away from the wall
            Vector2 directionTowardsWall = CurrentState == State.WallRight ? Vector2.right : Vector2.left;
            bool steerAway = Vector2.Angle(moveDirection, directionTowardsWall) > 140;

            bool notSteeringForward = moveDirection.y < 0.01f;
            
            // if any of that is the case exit out
            if (failed || steerAway || notSteeringForward) {
                NextMovementState(moveSet);
                return;
            }
            
            // Update Velocity

            // Rotate along wall
            Transform transform = moveSet.transform;
            transform.right = Vector3.Lerp(transform.right, CurrentState == State.WallRight ? - hit.normal : hit.normal, _lerpPercentage);
            
            // Move towards the wall
            Vector3 currentPosition = transform.position;
            Vector3 targetPosition = currentPosition - hit.normal * (hit.distance - _wallRunDistance);
            Vector3 wallLerp = Vector3.Lerp(currentPosition, targetPosition, _lerpPercentage);
            moveSet.Rigidbody.MovePosition(wallLerp);
            
            // Move along the wall
            moveSet.Rigidbody.velocity = transform.forward * moveSet.MovementSpeedManager.SpeedCap;

        }

        private State WallTest(MoveSet moveSet, out RaycastHit hit) {

            // Calculate necessary vectors
            Vector3 currentDirection = moveSet.Rigidbody.velocity.xOz();
            Quaternion rightRotation = Quaternion.AngleAxis(90, Vector3.up);
            Vector3 rightDirection = rightRotation * currentDirection;

            // Handle Right
            Ray rayRight = new Ray(moveSet.transform.position, rightDirection);
            if(Physics.Raycast(rayRight, out hit, _wallDetectionDistance, _wallLayer))
                return State.WallRight;

            // Handle Left
            Ray rayLeft = new Ray(moveSet.transform.position, - rightDirection);
            if (Physics.Raycast(rayLeft, out hit, _wallDetectionDistance, _wallLayer))
                return State.WallLeft;

            return State.Failed;
        }

        private void NextMovementState(MoveSet moveSet) {
            moveSet.SetMovementState<MovementStateGround>();
            moveSet.SetMovementState<MovementStateAir>();
        }
    }

}