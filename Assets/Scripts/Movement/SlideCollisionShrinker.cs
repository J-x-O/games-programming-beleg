using System;
using GameProgramming.Utility.TypeBasedEventSystem;
using UnityEngine;

namespace GameProgramming.Movement {

    [RequireComponent(typeof(CapsuleCollider))]
    [RequireComponent(typeof(MoveSet))]
    public class SlideCollisionShrinker : MonoBehaviour, ITypeBasedEventHandler<MovementState> {

        [Tooltip("How long it takes the Collider to shrink and go back to its original size")]
        [Range(0,1)]
        [SerializeField] private float _scaleSize = 0.3f;
        
        [Tooltip("How long it takes the Collider to shrink and go back to its original size")]
        [SerializeField] private float _scaleDuration = 0.2f;
        
        // cached refs
        private CapsuleCollider _collider;
        private MoveSet _moveSet;

        // the baseHeight the collider starts with
        private float _baseHeight;

        // the tween responsible for scaling the collider
        private LTDescr _scaleTween;
        
        private void Awake() {
            _collider = GetComponent<CapsuleCollider>();
            _baseHeight = _collider.height;
            _moveSet = GetComponent<MoveSet>();
        }

        private void OnEnable()
            => _moveSet.EventSystem.RegisterHandler(this, typeof(MovementStateSliding));

        private void OnDisable()
            => _moveSet.EventSystem.UnregisterHandler(this, typeof(MovementStateSliding));

        /// <summary> Shrink the collider </summary>
        public void HandleTypeBasedEventStart(MovementState instanceOfType) {
            float startHeight = _collider.height;
            if(_scaleTween != null) LeanTween.cancel(_scaleTween.uniqueId);
            _scaleTween = LeanTween.value(0, 1, _scaleDuration)
                .setOnUpdate((value) => {
                    _collider.height = Mathf.Lerp(startHeight, _baseHeight * _scaleSize, value);
                    
                    Vector3 center = _collider.center;
                    center.y = Mathf.Lerp(0, - _baseHeight * _scaleSize, value);
                    _collider.center = center;
                })
                .setOnComplete(() => _scaleTween = null);
        }

        /// <summary> Expand the collider </summary>
        public void HandleTypeBasedEventEnd(MovementState instanceOfType) {
            float startHeight = _collider.height;
            float startY = _collider.center.y;
            if(_scaleTween != null) LeanTween.cancel(_scaleTween.uniqueId);
            _scaleTween = LeanTween.value(0, 1, _scaleDuration)
                .setOnUpdate((value) => {
                    _collider.height = Mathf.Lerp(startHeight, _baseHeight, value);
                    Vector3 center = _collider.center;
                    center.y = Mathf.Lerp(startY, 0, value);
                    _collider.center = center;
                })
                .setOnComplete(() => _scaleTween = null);
        }
    }
}