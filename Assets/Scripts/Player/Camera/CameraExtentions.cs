using Cinemachine;
using UnityEngine;
using Utility;

namespace GameProgramming.Player.Camera {
    
    /// <summary> Adds Smooth Camera Binding Mode Changes </summary>
    public static class CameraExtentions {
        
        /// <summary> Changes the Binding Mode of the Camera, with handling the clean up for a clean transition </summary>
        /// <param name="mode"> The targeted Mode, not all supported yet </param>
        public static void ApplyBindingMode(this CinemachineFreeLook camera, CinemachineTransposer.BindingMode mode) {
            if (camera.Follow == null) return;
            float value;
            switch (mode) {
                case CinemachineTransposer.BindingMode.WorldSpace :
                    value = SwitchToWorldBinding(camera);
                    break;
                case CinemachineTransposer.BindingMode.SimpleFollowWithWorldUp :
                    value = SwitchToSimpleFollow(camera);
                    break;
                case CinemachineTransposer.BindingMode.LockToTarget :
                    value = SwitchToLookToTarget(camera);
                    break;
                default:
                    Debug.LogError($"Switching the Camera to BindingMode '{mode}' at runtime isn't implemented yet");
                    return;
            }
            camera.InternalUpdateCameraState(Vector3.up, -1);
            camera.m_XAxis.Value = value;
            camera.PreviousStateIsValid = false;
        }
        
        private static float SwitchToWorldBinding(CinemachineFreeLook camera) {
            Vector3 offset = (camera.State.CorrectedPosition - camera.Follow.position).xOz();
            float value = Vector3.SignedAngle(Vector3.back, offset, Vector3.up);
            camera.m_BindingMode = CinemachineTransposer.BindingMode.WorldSpace;
            return value;
        }

        private static float SwitchToSimpleFollow(CinemachineFreeLook camera) {
            camera.m_BindingMode = CinemachineTransposer.BindingMode.SimpleFollowWithWorldUp;
            return 0;
        }

        private static float SwitchToLookToTarget(CinemachineFreeLook camera) {
            Vector3 offset = (camera.State.CorrectedPosition - camera.Follow.position).xOz();
            float value = Vector3.SignedAngle(- camera.Follow.forward.xOz(), offset, Vector3.up);
            camera.m_BindingMode = CinemachineTransposer.BindingMode.LockToTarget;
            return value;
        }
    }
}