using System;
using Cinemachine;
using LevelObjects;
using UnityEngine;

namespace GameProgramming.Player.Camera {

    [RequireComponent(typeof(CinemachineVirtualCameraBase))]
    public class GoalCameraManager : MonoBehaviour {

        private CinemachineVirtualCameraBase _camera;

        private void Awake() => _camera = GetComponent<CinemachineVirtualCameraBase>();
        private void OnEnable() => Goal.OnLevelFinished += HandleOnLevelFinished;
        private void OnDisable() => Goal.OnLevelFinished -= HandleOnLevelFinished;

        private void HandleOnLevelFinished() => _camera.Follow = null;
    }

}