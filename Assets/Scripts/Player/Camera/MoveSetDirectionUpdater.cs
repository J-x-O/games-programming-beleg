using GameProgramming.Movement;
using UnityEngine;

namespace GameProgramming.Player.Camera {

    /// <summary> A Class that automatically updates the LookDirection on the linked MoveSet </summary>
    /// <remarks> This is based on the current direction and is intended to be on the Camera </remarks>
    public class MoveSetDirectionUpdater : MonoBehaviour {
        
        [Tooltip("The MoveSet where the LookDirectionValue will be updated")]
        [SerializeField] private MoveSet _moveSet;

        private void Update() {
            Vector3 currentDirection = _moveSet.transform.position - transform.position;
            _moveSet.CurrentLookDirection = new Vector2(currentDirection.x, currentDirection.z).normalized;
        }
    }

}