using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using Utility.BoolVector;

namespace GameProgramming.Player.Camera {

    // https://stuartspixelgames.com/2020/07/26/how-to-do-easy-saving-loading-with-binary-unity-c/
    
    public static class CameraSettings {

        private static readonly string _path = Application.persistentDataPath + "/CameraSettings.data";
        
        /// <summary> Writes camera settings to a binary file </summary>
        /// <param name="data"> The data that will be saved </param>
        public static void SaveSetting(CameraData data) {
            Debug.Log(_path);
            FileStream dataStream = new FileStream(_path, FileMode.Create);

            BinaryFormatter converter = new BinaryFormatter();
            converter.Serialize(dataStream, data);

            dataStream.Close();
        }

        /// <summary> Loads camera settings from a binary file </summary>
        /// <param name="data"> The loaded Data </param>
        /// <returns> If the data was successfully loaded </returns>
        public static bool LoadSettings(out CameraData data) {
            
            Debug.Log(_path);
            
            if (!File.Exists(_path)) {
                data = null;
                return false;
            }

            FileStream dataStream = new FileStream(_path, FileMode.Open);

            BinaryFormatter converter = new BinaryFormatter();
            data = converter.Deserialize(dataStream) as CameraData;

            dataStream.Close();
            return data != null;
        }
        
    }

    [Serializable]
    public class CameraData {
        public bool InvertX;
        public bool InvertY;
        public float SensitivityX;
        public float SensitivityY;
    }

}