using System;
using Cinemachine;
using UnityEngine;

namespace GameProgramming.Player.Camera {

    [RequireComponent(typeof(CinemachineFreeLook))]
    public class LoadCameraSettings : MonoBehaviour {

        private CinemachineFreeLook _camera;

        private void Awake() => _camera = GetComponent<CinemachineFreeLook>();

        private void Start() => UpdateSettings();

        public void UpdateSettings() {
            // If we could load Settings apply them
            if (CameraSettings.LoadSettings(out CameraData data)) {
                _camera.m_XAxis.m_MaxSpeed = data.SensitivityX;
                _camera.m_YAxis.m_MaxSpeed = data.SensitivityY;
                _camera.m_XAxis.m_InvertInput = data.InvertX;
                _camera.m_YAxis.m_InvertInput = data.InvertY;
            }
        }
    }

}