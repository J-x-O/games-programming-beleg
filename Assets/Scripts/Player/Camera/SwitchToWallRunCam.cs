using System;
using System.Collections;
using Cinemachine;
using GameProgramming.Movement;
using GameProgramming.Utility.TypeBasedEventSystem;
using UnityEngine;
using Utility;

namespace GameProgramming.Player.Camera {
    
    [RequireComponent(typeof(CinemachineFreeLook))]
    public class SwitchToWallRunCam : MonoBehaviour, ITypeBasedEventHandler<MovementState> {

        public event Action OnCameraTransition;
        
        private CinemachineFreeLook _camera;
        
        [Tooltip("The MoveSet that drives when the CameraRotation will be adjusted")]
        [SerializeField] private MoveSet _moveSet;

        [Tooltip("The BaseCam of the Player that will be active, when nothing is happening")]
        [SerializeField] private CinemachineTransposer.BindingMode _baseMode;
        
        [Tooltip("The WallRunCam, that will be used when the Player is running on a wall")]
        [SerializeField] private CinemachineTransposer.BindingMode _wallRunMode;

        private void Awake() {
            _camera = GetComponent<CinemachineFreeLook>();
        }

        protected void OnEnable() {
            _moveSet.EventSystem.RegisterHandler(this, typeof(MovementStateWallRun));
            HandleTypeBasedEventEnd(); //Reset Cameras
        }
        
        private void OnDisable() {
            _moveSet.EventSystem.UnregisterHandler(this, typeof(MovementStateWallRun));
            HandleTypeBasedEventEnd(); //Reset Cameras
        }

        /// <summary> Called when the Player starts wall running, activates the right cam based on the side </summary>
        /// <param name="instanceOfType"> the active instance, passed in by the event system </param>
        public void HandleTypeBasedEventStart(MovementState instanceOfType) {

            // resolve the state and switch to the right camera
            MovementStateWallRun.State state = ((MovementStateWallRun) instanceOfType).CurrentState;
            switch (state) {
                case MovementStateWallRun.State.WallRight:
                    _camera.ApplyBindingMode(_wallRunMode);
                    _camera.m_XAxis.m_MaxValue = 180;
                    _camera.m_XAxis.m_MinValue = 0;
                    _camera.m_XAxis.m_Wrap = false;
                    OnCameraTransition?.Invoke();
                    break;

                case MovementStateWallRun.State.WallLeft:
                    _camera.ApplyBindingMode(_wallRunMode);
                    _camera.m_XAxis.m_MaxValue = 0;
                    _camera.m_XAxis.m_MinValue = -180;
                    _camera.m_XAxis.m_Wrap = false;
                    OnCameraTransition?.Invoke();
                    break;

                default: HandleTypeBasedEventEnd(); break;
            }
        }

        /// <summary> Reset all cams back to the base cam </summary>
        /// /// <param name="instanceOfType"> not necessary for this function </param>
        public void HandleTypeBasedEventEnd(MovementState instanceOfType = null) {
            _camera.ApplyBindingMode(_baseMode);
            _camera.m_XAxis.m_MaxValue = 180;
            _camera.m_XAxis.m_MinValue = -180;
            _camera.m_XAxis.m_Wrap = true;
            OnCameraTransition?.Invoke();
        }
    }
}