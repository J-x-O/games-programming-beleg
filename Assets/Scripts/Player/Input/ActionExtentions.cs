using System;
using UnityEngine.InputSystem;

namespace GameProgramming.Player.Input {

    public static class ActionExtentions {
        
        public static void SubscribeToAll(this InputAction action, params Action<InputAction.CallbackContext>[] linked) {
            foreach (Action<InputAction.CallbackContext> link in linked) {
                action.started += link;
                action.performed += link;
                action.canceled += link;
            }

        }
        
        public static void UnsubscribeFromAll(this InputAction action, params Action<InputAction.CallbackContext>[] linked) {
            foreach (Action<InputAction.CallbackContext> link in linked) {
                action.started -= link;
                action.performed -= link;
                action.canceled -= link;
            }
        }
        
    }

}