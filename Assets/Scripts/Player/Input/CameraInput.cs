using System;
using Cinemachine;
using GameProgramming.Movement;
using UnityEngine;
using UnityEngine.InputSystem;
using Utility.BoolVector;

namespace GameProgramming.Player.Input {

    public class CameraInput : MonoBehaviour, AxisState.IInputAxisProvider {

        private Vector2 _movementCache = Vector2.zero;
        
        private void OnEnable()
            => InputMaster.PlayerControls.InGame.Look.SubscribeToAll(CacheMovement);

        private void OnDisable()
            => InputMaster.PlayerControls.InGame.Look.UnsubscribeFromAll(CacheMovement);

        private void CacheMovement(InputAction.CallbackContext context)
            => _movementCache += context.ReadValue<Vector2>();

        /// <summary> Reads the action associated with the axis </summary>
        /// <param name="axis"> Index ranges from 0 to 1 for X and Y </param>
        /// <returns> The current axis value </returns>
        public float GetAxisValue(int axis) {

            // If we are pausing we dont want to move the camera
            if (PauseManager.Pausing) return 0;
            
            // Get the Value
            Vector2 value = _movementCache;

            // Empty the Cache, return the right value
            switch (axis) {
                case 0 :
                    _movementCache.x = 0;
                    return value.x;
                case 1 :
                    _movementCache.y = 0;
                    return value.y;
                default :
                    return 0;
            }
        }
    }
}