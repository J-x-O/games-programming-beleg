using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameProgramming.Player.Input {

    public class InputMaster : MonoBehaviour {

        /// <summary> An Instance of the NewInputSystem Asset responsible for Invoking all C# Events </summary>
        public static PlayerControls PlayerControls => _playerControls ??= new PlayerControls();
        private static PlayerControls _playerControls;
        
        private void Awake() {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        private void OnEnable() {
            PlayerControls.Enable();
        }

        private void OnDisable() {
            PlayerControls.Disable();
        }
    }

}


