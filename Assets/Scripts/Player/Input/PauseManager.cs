using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace GameProgramming.Player.Input {

    public class PauseManager : MonoBehaviour {

        public static event Action OnPause;

        public static event Action OnUnpause;

        public static bool Pausing { get; private set; }
        
        private void OnEnable() => InputMaster.PlayerControls.InGame.Pause.started += HandlePause;

        private void OnDisable() => InputMaster.PlayerControls.InGame.Pause.started -= HandlePause;

        private void HandlePause(InputAction.CallbackContext context) {
            if (Pausing) Unpause();
            else Pause();
        }

        public static void Pause() {
            if (Pausing) return;
            Pausing = true;
            OnPause?.Invoke();
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        public static void Unpause() {
            if (!Pausing) return;
            Pausing = false;
            OnUnpause?.Invoke();
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }
}