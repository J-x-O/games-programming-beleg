using System;
using GameProgramming.Movement;
using UnityEngine;
using UnityEngine.InputSystem;

namespace GameProgramming.Player.Input {

    [RequireComponent(typeof(MoveSet))]
    public class PlayerController : MonoBehaviour {
        
        /// <summary> A Ref to the <see cref="MoveSet"/> of the Player </summary>
        private MoveSet _moveSet;
        
        private void Start() {
            _moveSet = GetComponent<MoveSet>();
            Walk();
        }

        private void OnEnable() {
            PlayerControls.InGameActions actions = InputMaster.PlayerControls.InGame;
            actions.Jump.started += Jump;
            actions.Slide.started += StartSlide;
            actions.Slide.canceled += Walk;
            actions.Move.SubscribeToAll(HandleMovement);
        }

        private void OnDisable() {
            PlayerControls.InGameActions actions = InputMaster.PlayerControls.InGame;
            actions.Jump.started -= Jump;
            actions.Slide.started -= StartSlide;
            actions.Move.UnsubscribeFromAll(HandleMovement);
        }

        private void Walk() {
            _moveSet.SetMovementState<MovementStateGround>();
            _moveSet.SetMovementState<MovementStateAir>();
        }
        
        private void Walk(InputAction.CallbackContext context) => Walk();

        /// <summary> Try to wall run, if not possible jump </summary>
        /// <param name="context"> CallbackContext of the InputSystem </param>
        private void Jump(InputAction.CallbackContext context) {
            if (_moveSet.CurrentMovementState.GetType() != typeof(MovementStateWallRun))
                if(_moveSet.SetMovementState<MovementStateWallRun>()) return;
            
            _moveSet.SetMovementState<MovementStateJumping>();
        }

        private void StartSlide(InputAction.CallbackContext context) {
            _moveSet.SetMovementState<MovementStateSliding>();
        }

        /// <summary> Try activating both states of Running </summary>
        /// <param name="context"> CallbackContext of the InputSystem </param>
        private void HandleMovement(InputAction.CallbackContext context)
            => _moveSet.CurrentMoveDirection = context.ReadValue<Vector2>();
    }
}
