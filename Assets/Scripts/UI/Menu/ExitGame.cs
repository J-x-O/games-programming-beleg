using UnityEngine;

namespace UI.Menu {

    public class ExitGame : MonoBehaviour {

        public void ShutdownEngine() => Application.Quit();
    }

}