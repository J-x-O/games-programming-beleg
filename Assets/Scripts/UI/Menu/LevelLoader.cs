using GameProgramming.Player.Input;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI.Menu {

    public class LevelLoader : MonoBehaviour {

        public void LoadLevel(string identifier) {
            PauseManager.Unpause();
            SceneManager.LoadScene(identifier);
        }

        public void SetCursorModeMainMenu() {
            Cursor.lockState = CursorLockMode.None; 
            Cursor.visible = true;
        }
    }
}