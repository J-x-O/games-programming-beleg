using System;
using System.Collections.Generic;
using System.Linq;
using Cinemachine;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI.Menu {
    
    public class LightManager : MonoBehaviour {

        [Tooltip("The Light that will be rotated towards the target")]
        [SerializeField] private Light _target;
        
        [Tooltip("The initial Target this Light will immediately point to")]
        [SerializeField] private string _initialTarget;
        
        [Serializable]
        struct NamedTarget {
            public string Identifier;
            public Transform LookAtTarget;
        }

        [Tooltip("All Transforms we can point towards via their identifier")]
        [SerializeField] private List<NamedTarget> _transforms;

        private Dictionary<string, Transform> _dictionary;

        [Tooltip("How long it takes the light to rotate")]
        [SerializeField] private float _rotationTime = 0.5f;

        private LTDescr _rotateTween;

        private void Awake() {
            _dictionary = new Dictionary<string, Transform>();
            foreach (NamedTarget target in _transforms) {
                _dictionary.Add(target.Identifier, target.LookAtTarget);
                if(target.Identifier == _initialTarget) _target.transform.LookAt(target.LookAtTarget);
            }
        }

        public void SwitchToLight(string identifier) {
            
            // A Dictionary would be faster, but since we have to iterate over the cams anyways
            // this would just make it overly complicated for a slightly faster search
            if (!_dictionary.ContainsKey(identifier)) {
                Debug.LogWarning($"Event with the identifier {identifier} couldn't be found");
                return;
            }
            
            // Calculate Rotation Variables
            Transform targetTransform = _target.transform;
            Vector3 currentForward = targetTransform.position + targetTransform.forward;
            Vector3 targetForward = _dictionary[identifier].position;
            
            // Rotate towards the new Target
            if(_rotateTween != null) LeanTween.cancel(_rotateTween.uniqueId);
            _rotateTween = LeanTween.value(0, 1, _rotationTime)
                .setEaseInOutSine()
                .setOnUpdate((value) => {
                    if(targetTransform != null) targetTransform.LookAt(Vector3.Lerp(currentForward, targetForward, value));
                });
        }
        
    }

}