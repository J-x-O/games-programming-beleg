using System;
using GameProgramming.Player.Camera;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utility.BoolVector;

namespace UI.Menu {

    public class SaveCameraSliderSettings : MonoBehaviour {
        
        [SerializeField] private Slider _xSlider;
        [SerializeField] private Slider _ySlider;

        [SerializeField] private Vector2 _baseMultiplier = new Vector2(0.006f, 0.45f);
        
        Vector2 _currentValue = Vector2.zero;

        private void Start() {
            if (!CameraSettings.LoadSettings(out CameraData data)) return;
            _xSlider.value = data.SensitivityX / _baseMultiplier.x;
            _ySlider.value = data.SensitivityY / _baseMultiplier.y;
        }

        private void OnEnable() {
            _xSlider.onValueChanged.AddListener(HandleOnXValueChanged);
            _ySlider.onValueChanged.AddListener(HandleOnYValueChanged);
        }

        private void OnDisable() {
            _xSlider.onValueChanged.RemoveListener(HandleOnXValueChanged);
            _ySlider.onValueChanged.RemoveListener(HandleOnYValueChanged);
        }

        private void HandleOnXValueChanged(float value) {
            _currentValue.x = value * _baseMultiplier.x;
            SaveSettings();
        }
        
        private void HandleOnYValueChanged(float value) {
            _currentValue.y = value * _baseMultiplier.y;
            SaveSettings();
        }

        private void SaveSettings() {
            CameraSettings.SaveSetting(new CameraData() {
                SensitivityX = _currentValue.x,
                SensitivityY = _currentValue.y,
                InvertX = false,
                InvertY = true,
            });
        }
        
    }

}