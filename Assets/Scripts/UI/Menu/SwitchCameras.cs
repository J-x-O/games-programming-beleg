using System;
using System.Collections.Generic;
using System.Linq;
using Cinemachine;
using UnityEngine;

namespace Menu {

    public class SwitchCameras : MonoBehaviour {

        [Tooltip("All Cameras we can switch to via their identifier")]
        [SerializeField] private string _firstTarget;
        
        [Serializable]
        struct NamedCamera {
            public string Identifier;
            public CinemachineVirtualCameraBase Camera;
        }

        [Tooltip("All Cameras we can switch to via their identifier")]
        [SerializeField] private List<NamedCamera> _cameras;

        private void Awake() => SwitchToCamera(_firstTarget);

        public void SwitchToCamera(string identifier) {
            
            // A Dictionary would be faster, but since we have to iterate over the cams anyways
            // this would just make it overly complicated for a slightly faster search
            if (!_cameras.Select((cam) => cam.Identifier).Contains(identifier)) {
                Debug.LogWarning($"Event with the identifier {identifier} couldn't be found");
                return;
            }
            
            // Activate the targeted camera
            foreach (NamedCamera cam in _cameras) {
                cam.Camera.enabled = cam.Identifier == identifier;
            }
        }
    }
}