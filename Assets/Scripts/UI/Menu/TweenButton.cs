using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

namespace UI {

    public class TweenButton : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler {

        [Header("Hover")]
        [SerializeField] private float _scaleFactorHover = 1.1f;
        [SerializeField] private float _scaleTimeHover = 0.1f;
        
        [Header("Click")]
        [SerializeField] private float _scaleFactorClick = 0.9f;
        [SerializeField] private float _scaleTimeClick = 0.1f;
        
        [Space(10)]
        [Tooltip("An Event being invoked when this button is pressed")]
        [SerializeField] private UnityEvent _onClick;
        
        /// <summary> An Event being invoked when this button is pressed </summary>
        public event Action<PointerEventData> OnClick;
        
        private LTDescr _scaleTween;
        private Vector3 _startSize;

        private void Awake() {
            _startSize = transform.localScale;
        }

        /// <summary> Scale small, then scale back to normal </summary>
        /// <param name="eventData"> EventData provided by the event system </param>
        public void OnPointerClick(PointerEventData eventData) {
            
            // First scale to small, then back
            if(_scaleTween != null) LeanTween.cancel(_scaleTween.uniqueId);
            _scaleTween = LeanTween.scale(gameObject, _startSize * _scaleFactorClick, _scaleTimeClick)
                .setEaseInOutSine()
                .setLoopPingPong(1);

            // Invoke the Event
            _onClick?.Invoke();
            OnClick?.Invoke(eventData);
        }

        /// <summary> Scale small </summary>
        /// <param name="eventData"> EventData provided by the event system </param>
        public void OnPointerEnter(PointerEventData eventData) {
            if(_scaleTween != null) LeanTween.cancel(_scaleTween.uniqueId);
            _scaleTween = LeanTween.scale(gameObject, _startSize * _scaleFactorHover, _scaleTimeHover)
                .setEaseInOutSine();
        }

        /// <summary> Scale back to normal</summary>
        /// <param name="eventData"> EventData provided by the event system </param>
        public void OnPointerExit(PointerEventData eventData) {
            if(_scaleTween != null) LeanTween.cancel(_scaleTween.uniqueId);
            _scaleTween = LeanTween.scale(gameObject, _startSize, _scaleTimeHover)
                .setEaseInOutSine();
        }
    }
}