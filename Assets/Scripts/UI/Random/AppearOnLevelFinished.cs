using System;
using LevelObjects;
using UnityEngine;

namespace UI.Random {

    public class AppearOnLevelFinished : MonoBehaviour {
        private void Awake() {
            for (int i = 0; i < transform.childCount; i++) {
                transform.GetChild(i).gameObject.SetActive(false);
            }
        }

        private void OnEnable() => Goal.OnLevelFinished += HandleOnLevelFinished;
        private void OnDisable() => Goal.OnLevelFinished -= HandleOnLevelFinished;

        private void HandleOnLevelFinished() {
            for (int i = 0; i < transform.childCount; i++) {
                transform.GetChild(i).gameObject.SetActive(true);
            }
        }
    }

}