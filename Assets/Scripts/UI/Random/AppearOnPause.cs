using System;
using GameProgramming.Player.Input;
using LevelObjects;
using UnityEngine;

namespace UI.Random {

public class AppearOnPause : MonoBehaviour {
    
    private void Awake() => DisableAll();

    private void OnEnable() {
            PauseManager.OnPause += EnableAll;
            PauseManager.OnUnpause += DisableAll;
        }

        private void OnDisable() {
            PauseManager.OnPause -= EnableAll;
            PauseManager.OnUnpause -= DisableAll;
        }

        private void EnableAll() {
            for (int i = 0; i < transform.childCount; i++) {
                transform.GetChild(i).gameObject.SetActive(true);
            }
        }
        
        private void DisableAll() {
            for (int i = 0; i < transform.childCount; i++) {
                transform.GetChild(i).gameObject.SetActive(false);
            }
        }
    }
}
