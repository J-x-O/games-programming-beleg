using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI.Random {

    [RequireComponent(typeof(TweenButton))]
    [RequireComponent(typeof(Rigidbody))]
    public class ApplyForceOnClick : MonoBehaviour {

        [Tooltip("How much force will be applied to the Object")]
        [SerializeField] private float _forceStrength = 5;

        [Tooltip("The Radius of the Force that will be applied")]
        [SerializeField] private float _radius = 1;
        
        private TweenButton _button;
        private Rigidbody _rigidbody;
        
        private void Awake() {
            _button = GetComponent<TweenButton>();
            _rigidbody =  GetComponent<Rigidbody>();
        }

        private void OnEnable() => _button.OnClick += HandleOnClick;
        private void OnDisable() => _button.OnClick -= HandleOnClick;

        private void HandleOnClick(PointerEventData data) {
            Debug.Log(data.pointerCurrentRaycast.worldPosition);
            _rigidbody.AddExplosionForce(_forceStrength, data.pointerCurrentRaycast.worldPosition, _radius);
            
        }
    }

}