using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Random {

    [RequireComponent(typeof(TMP_Text))]
    public class SliderListener : MonoBehaviour {

        [SerializeField] private Slider _target;

        [SerializeField] private string _prefix;
        
        private TMP_Text _text;

        private void Awake() => _text = GetComponent<TMP_Text>();

        private void OnEnable() => _target.onValueChanged.AddListener(HandleOnValueChanged);

        private void OnDisable() => _target.onValueChanged.RemoveListener(HandleOnValueChanged);

        private void HandleOnValueChanged(float value) {
            _text.text = _prefix + value.ToString("0.000");
        }
    }

}