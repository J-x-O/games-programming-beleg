using System;
using GameProgramming.Movement;
using TMPro;
using UnityEngine;

namespace UI.Visualizer {

    public class MovementSpeedVisualizer : MonoBehaviour {

        [Tooltip("The Target of which we will display the information")]
        [SerializeField] private MovementSpeedManager _target;

        [SerializeField] private float _sizeMultiplier;

        [Header("Refferences")]
        [SerializeField] private RectTransform _currentSpeedBox;
        [SerializeField] private RectTransform _currentMaxSpeedBox;
        [SerializeField] private RectTransform _additionalSpeedBox;
        [SerializeField] private RectTransform _targetMaxSpeedBox;

        [SerializeField] private TMP_Text _currentSpeedText;
        [SerializeField] private TMP_Text _currentMaxSpeedText;
        [SerializeField] private TMP_Text _additionalSpeedText;
        [SerializeField] private TMP_Text _targetMaxSpeedText;
        
        private void OnEnable() {
            _target.OnMovementDataChanged += HandleOnMovementDataChanged;
        }
        
        private void OnDisable() {
            _target.OnMovementDataChanged -= HandleOnMovementDataChanged;
        }

        private void HandleOnMovementDataChanged(MovementUpdateData movementUpdateData) {
            _currentSpeedBox.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, movementUpdateData.CurrentSpeed * _sizeMultiplier);
            _currentMaxSpeedBox.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, movementUpdateData.CurrentMaxSpeed * _sizeMultiplier);
            _additionalSpeedBox.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, movementUpdateData.CurrentAdditionalSpeed * _sizeMultiplier);
            _targetMaxSpeedBox.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, movementUpdateData.TargetMaxSpeed * _sizeMultiplier);
            
            _currentSpeedText.text = movementUpdateData.CurrentSpeed.ToString("0.00");
            _currentMaxSpeedText.text = movementUpdateData.CurrentMaxSpeed.ToString("0.00");
            _additionalSpeedText.text = movementUpdateData.CurrentAdditionalSpeed.ToString("0.00");
            _targetMaxSpeedText.text = movementUpdateData.TargetMaxSpeed.ToString("0.00");
        }
    }

}