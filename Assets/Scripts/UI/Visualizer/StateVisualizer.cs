using System;
using System.Linq;
using System.Reflection;
using GameProgramming.Movement;
using GameProgramming.Utility.TypeBasedEventSystem;
using TMPro;
using UnityEngine;

namespace UI.Visualizer {

    [RequireComponent(typeof(TMP_Text))]
    public class StateVisualizer : MonoBehaviour, ITypeBasedEventHandler<MovementState> {

        [Tooltip("The MoveSet of which we will display the State")]
        [SerializeField] private MoveSet _target;
        
        private TMP_Text _text;

        private void Start() {
            _text = GetComponent<TMP_Text>();
            _target.EventSystem.RegisterHandler(this, GetAllSubTypes<MovementState>());
        }
        
        private Type[] GetAllSubTypes<TType>() {
            return Assembly.GetAssembly(typeof(TType)).GetTypes()
                .Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(TType)))
                .ToArray();
        }

        public void HandleTypeBasedEventStart(MovementState instanceOfType) {
            _text.text = instanceOfType.GetType().ToString();
        }

        public void HandleTypeBasedEventEnd(MovementState instanceOfType) {
            _text.text = "No State";
        }
    }

}