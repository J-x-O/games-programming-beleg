using System;

namespace Utility.BoolVector {

    [Serializable]
    public struct BoolVector2 {
        public bool x;
        public bool y;

        public BoolVector2(bool x, bool y) {
            this.x = x;
            this.y = y;
        }
    }

}