using System;

namespace Utility.BoolVector {

    [Serializable]
    public struct BoolVector3 {
        public bool x;
        public bool y;
        public bool z;
        
        public BoolVector3(bool x, bool y, bool z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }

}