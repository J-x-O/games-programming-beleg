using System;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Utility.BoolVector.Editor {

    [CustomPropertyDrawer(typeof(BoolVector2))]
    public class BoolVector2Drawer : PropertyDrawer {

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            
            EditorGUI.BeginProperty(position, label, property);
            
            // Draw Label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            
            // Calculate rects (Weird Numbers to align them with normal Vectors)
            Rect xLabelRect = new Rect(position.x, position.y, 10, position.height);
            Rect xRect = new Rect(xLabelRect.x + 12.5f, position.y, 30, position.height);
            Rect yLabelRect = new Rect(position.x + position.width / 3 + 1, position.y, 10, position.height);
            Rect yRect = new Rect(yLabelRect.x + 12.5f, position.y, 30, position.height);

            // Find Properties
            SerializedProperty xProperty = property.FindPropertyRelative("x");
            SerializedProperty yProperty = property.FindPropertyRelative("y");
            
            // Draw fields 
            EditorGUI.LabelField(xLabelRect, "X");
            xProperty.boolValue = EditorGUI.Toggle(xRect, xProperty.boolValue);
            EditorGUI.LabelField(yLabelRect, "Y");
            yProperty.boolValue = EditorGUI.Toggle(yRect, yProperty.boolValue);

            EditorGUI.EndProperty();
        }
    }

}