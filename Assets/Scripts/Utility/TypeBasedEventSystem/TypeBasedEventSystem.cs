﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace GameProgramming.Utility.TypeBasedEventSystem {
    
    /// <summary>The custom event system hooking into each <see cref="StatusEffectManager"/>.</summary>
    /// <remarks>Written by Max, Paul, Philipp</remarks>
    public class TypeBasedEventSystem<T> {

        private const int EVENT_START = 0, EVENT_END = 1;
        
        // list that keeps track of all events with their corresponding effect classtype
        private readonly Dictionary<Type, Action<T>[]> _typeEventHandlers;

        public TypeBasedEventSystem() {
            _typeEventHandlers = new Dictionary<Type, Action<T>[]>();
        }

        /// <summary>Registers a new event action linked to a fitting class type.</summary>
        /// <param name="subscribedClasses">All types extending <see cref="StatusEffectType" /> which will be listened to.</param>
        /// <param name="eventHandler">The custom event handler class.</param>
        public void RegisterHandler(ITypeBasedEventHandler<T> eventHandler, params Type[] subscribedClasses) {
            foreach (Type subscribedClass in subscribedClasses) {
                
                // check if valid type was given
                if (!subscribedClass.IsSubclassOf(typeof(T)))
                    throw new ArgumentException("The event handler type must be a specific status effect type.");

                // register new registry if no handler of the type was created yet
                if (!_typeEventHandlers.ContainsKey(subscribedClass)) _typeEventHandlers.Add(subscribedClass, new Action<T>[2]);

                // register native event actions
                _typeEventHandlers[subscribedClass][EVENT_START] += eventHandler.HandleTypeBasedEventStart;
                _typeEventHandlers[subscribedClass][EVENT_END] += eventHandler.HandleTypeBasedEventEnd;
            }
        }
        
        /// <summary>Unregisters an event handler of given status effect types.</summary>
        /// <param name="eventHandler">The event handler which will be unregistered.</param>
        /// <param name="subscribedClasses">The status effect types which the event handler will no longer listen to.</param>
        public void UnregisterHandler(ITypeBasedEventHandler<T> eventHandler, params Type[] subscribedClasses) {
            foreach (Type subscribedClass in subscribedClasses) {
                // check if valid type was given
                if (!subscribedClass.IsSubclassOf(typeof(T)))
                    throw new ArgumentException("The event handler type must be a specific status effect type.");

                // if no registry for the given effect type, no Action has to be unregistered
                if (!_typeEventHandlers.ContainsKey(subscribedClass)) continue;

                // unregister native event actions
                _typeEventHandlers[subscribedClass][EVENT_START] -= eventHandler.HandleTypeBasedEventStart;
                _typeEventHandlers[subscribedClass][EVENT_END] -= eventHandler.HandleTypeBasedEventEnd;
            }
        }

        /// <summary>Notifies all listeners about the start with the given effect`s type.</summary>
        /// <param name="invokedType">Effect that was started.</param>
        public void InvokeTypeBasedEventStart(T invokedType) {
            // do not invoke event again if already running
            //if (statusEffect.Duration > statusEffect.Type.BaseDuration) return;

            if (_typeEventHandlers.ContainsKey(invokedType.GetType()))
                _typeEventHandlers[invokedType.GetType()][EVENT_START]?.Invoke(invokedType);
        }
        
        /// <summary>Notifies all listeners about the start with the given effect`s type.</summary>
        /// <param name="invokedType">Effect that was started.</param>
        public void InvokeTypeBasedEventEnd(T invokedType) {
            // do not invoke event again if already running
            //if (statusEffect.Duration > statusEffect.Type.BaseDuration) return;

            if (_typeEventHandlers.ContainsKey(invokedType.GetType()))
                _typeEventHandlers[invokedType.GetType()][EVENT_END]?.Invoke(invokedType);
        }
    }
}