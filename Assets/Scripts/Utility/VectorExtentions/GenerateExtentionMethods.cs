// Exclude from Compile
#if (UNITY_EDITOR)

using System;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;

namespace Utility {
     
    /// <summary> A static class providing the functionality to generate all code for the ZeroSwizzles </summary>
    public static class GenerateExtentionMethods {

        private static string _path = Directory.GetCurrentDirectory() + "\\Assets\\Scripts\\Utility\\VectorExtentions\\VectorZeroSwizzleExtentions.cs";
        
        private enum Options { x, y, z, w, O }

        private enum VecType { Vector2, Vector3, Vector4 }

        private static VecType[] _allVecTypes = new[] {VecType.Vector2, VecType.Vector3, VecType.Vector4};
        
        [MenuItem("Tools/Generate Extension Methods")]
        public static void Generate() {
            using FileStream fileStream = new FileStream(_path, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
            using StreamWriter streamWriter = new StreamWriter(fileStream, Encoding.UTF8);
            
            streamWriter.WriteLine("using UnityEngine;");
            streamWriter.WriteLine("");
            streamWriter.WriteLine("namespace Utility {");
            
            SolveVecType(streamWriter, VecType.Vector2);
            SolveVecType(streamWriter, VecType.Vector3);
            SolveVecType(streamWriter, VecType.Vector4);
            
            streamWriter.WriteLine("}");
        }

        private static void SolveVecType(StreamWriter streamWriter, VecType type) {
            
            streamWriter.WriteLine($"    public static class {type.ToString()}ZeroSwizzles {{");
            
            streamWriter.WriteLine("         //swizzles of size 2");
            foreach (Options option1 in type.GetOptions()) {
                foreach (Options option2 in type.GetOptions()) {

                    Options[] currentOptions = {option1, option2};
                    
                    if(!CheckConditions(currentOptions)) continue;

                    string solveLine = SolveLine(VecType.Vector2, type, currentOptions);
                    streamWriter.WriteLine(solveLine);
                }
            }
            
            streamWriter.WriteLine("         //swizzles of size 3");
            foreach (Options option1 in type.GetOptions()) {
                foreach (Options option2 in type.GetOptions()) {
                    foreach (Options option3 in type.GetOptions()) {

                        Options[] currentOptions = {option1, option2, option3};
                        
                        if (!CheckConditions(currentOptions)) continue;

                        string solveLine = SolveLine(VecType.Vector3, type, currentOptions);
                        streamWriter.WriteLine(solveLine);
                    }
                }
            }
            
            streamWriter.WriteLine("         //swizzles of size 4");
            foreach (Options option1 in type.GetOptions()) {
                foreach (Options option2 in type.GetOptions()) {
                    foreach (Options option3 in type.GetOptions()) {
                        foreach (Options option4 in type.GetOptions()) {

                            Options[] currentOptions = {option1, option2, option3, option4};
                            
                            if (!CheckConditions(currentOptions)) continue;

                            string solveLine = SolveLine(VecType.Vector4, type, currentOptions);
                            streamWriter.WriteLine(solveLine);
                        }
                    }
                }
            }
            
            streamWriter.WriteLine("    }");
            streamWriter.WriteLine("");
        }

        /// <summary> Checks if we want this combination in out class </summary>
        /// <param name="options"> The options that will be checked </param>
        /// <returns> If this combination is valid or not and should be added </returns>
        private static bool CheckConditions(params Options[] options) {

            // Check if we have at least one 0, this could be removed,
            // then you wouldn't need to other class from the internet anymore
            if (!options.Contains(Options.O)) return false;
            
            // Check if all of them are 0
            if (options.All(option => option == Options.O)) return false;
            
            // If no we are good to go
            return true;
        }

        /// <summary> Solves a single line of the final file </summary>
        /// <param name="target"> The type that will be created from the Options </param>
        /// <param name="baseType"> The type that provides the options </param>
        /// <param name="options"> The specific combination of options for this line </param>
        /// <returns> The constructed string for this line </returns>
        private static string SolveLine(VecType target, VecType baseType, params Options[] options) {
            StringBuilder returnString = new StringBuilder();
            
            returnString.Append($"        public static {target.ToString()} ");
            
            foreach (Options option in options) {
                returnString.Append(option.ToString());
            }

            returnString.Append($"(this {baseType.ToString()} a) => new {target.ToString()}(");

            foreach (Options option in options) {
                returnString.Append(option != Options.O
                    ? $"a.{option.ToString()}, "
                    : "0, ");
            }
            
            // Remove the last comma
            returnString.Length -= 2;

            returnString.Append(");");

            return returnString.ToString();
        }

        /// <summary> Returns all legal options that can make up a Vector of that size </summary>
        /// <param name="type"> The type of Vector you want the options for </param>
        private static Options[] GetOptions(this VecType type) {
            return type switch {
                VecType.Vector2 => new[] {Options.x, Options.y, Options.O},
                VecType.Vector3 => new[] {Options.x, Options.y, Options.z, Options.O},
                VecType.Vector4 => new[] {Options.x, Options.y, Options.z, Options.w, Options.O},
                _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
            };
        }
    }
}
#endif