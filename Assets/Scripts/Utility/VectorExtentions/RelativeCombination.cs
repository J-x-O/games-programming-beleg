using UnityEngine;

namespace Utility {

    public static class RelativeCombination {
        
        /// <summary> A Method to calculate the combined direction in relation to another direction </summary>
        /// <param name="direction1"> The base direction </param>
        /// <param name="direction2"> The direction the first one is in relation too </param>
        /// <returns> The combined direction based of direction one in relation too direction two </returns>
        public static Vector2 RelativeCombine(this Vector2 direction1, Vector2 direction2) {
            
            // Rotate the moveDirection based on the Angle to the standard forward Vector ({0,0,1} but in 2D {0,1})
            Quaternion lookRotation = Quaternion.FromToRotation(new Vector2(0,1), direction2);
            return lookRotation * direction1;
        }
        
        /// <summary> A Method to calculate the combined direction in relation to another direction </summary>
        /// <param name="direction1"> The base direction </param>
        /// <param name="direction2"> The direction the first one is in relation too </param>
        /// <returns> The combined direction based of direction one in relation too direction two </returns>
        public static Vector3 RelativeCombine(this Vector3 direction1, Vector3 direction2) {
            
            // Rotate the moveDirection based on the Angle to the standard forward Vector
            Quaternion lookRotation = Quaternion.FromToRotation(new Vector3(0, 0, 1), direction2);
            return lookRotation * direction1;
        }
    }

}