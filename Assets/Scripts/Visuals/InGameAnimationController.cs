using System;
using System.Collections.Generic;
using System.Linq;
using GameProgramming.Movement;
using GameProgramming.Utility.TypeBasedEventSystem;
using UnityEngine;
using Utility;

namespace Visuals {

    [RequireComponent(typeof(Animator))]
    public class InGameAnimationController : MonoBehaviour, ITypeBasedEventHandler<MovementState> {
        
        private MoveSet _moveSet;

        private Animator _animator;
        
        // Cached hashes for performance
        private static readonly int SPEED = Animator.StringToHash("Speed");
        private static readonly int SLIDING = Animator.StringToHash("Sliding");
        private static readonly int JUMP = Animator.StringToHash("Jump");
        private static readonly int GROUNDED = Animator.StringToHash("Grounded");
        private static readonly int WALLRUN_RIGHT = Animator.StringToHash("WallrunRight");
        private static readonly int WALLRUN_LEFT = Animator.StringToHash("WallrunLeft");
        
        Dictionary<Type, int> _typeDict = new Dictionary<Type, int> {
            {typeof(MovementStateSliding), 0},
            {typeof(MovementStateJumping), 1},
            {typeof(MovementStateWallRun), 2}
        };
        
        private void Awake() {
            _moveSet = GetComponentInParent<MoveSet>();
            _animator = GetComponent<Animator>();
        }

        private void OnEnable() {
            _moveSet.EventSystem.RegisterHandler(this, _typeDict.Keys.ToArray());
            _moveSet.GroundCheck.OnGrounded += SetAnimatorGrounded;
            _moveSet.GroundCheck.OnUngrounded += SetAnimatorUngrounded;
        }

        private void OnDisable() {
            _moveSet.EventSystem.UnregisterHandler(this, _typeDict.Keys.ToArray());
            _moveSet.GroundCheck.OnGrounded -= SetAnimatorGrounded;
            _moveSet.GroundCheck.OnUngrounded -= SetAnimatorUngrounded;
        }
        
        private void Update() {
            _animator.SetFloat(SPEED, _moveSet.Rigidbody.velocity.xz().magnitude);
        }

        private void SetAnimatorGrounded() => _animator.SetBool(GROUNDED, true);

        private void SetAnimatorUngrounded() => _animator.SetBool(GROUNDED, false);

        public void HandleTypeBasedEventStart(MovementState instanceOfType) {

            switch (_typeDict[instanceOfType.GetType()]) {
                case 0 : _animator.SetBool(SLIDING, true); break;
                case 1 : _animator.SetTrigger(JUMP); break;
                case 2 :
                    switch (((MovementStateWallRun)instanceOfType).CurrentState) {
                        case MovementStateWallRun.State.WallLeft: _animator.SetBool(WALLRUN_LEFT, true); break;
                        case MovementStateWallRun.State.WallRight:_animator.SetBool(WALLRUN_RIGHT, true); break;
                    }
                    break;
            }
        }

        public void HandleTypeBasedEventEnd(MovementState instanceOfType) {

            switch (_typeDict[instanceOfType.GetType()]) {
                case 0: _animator.SetBool(SLIDING, false); break;
                case 2:
                    _animator.SetBool(WALLRUN_LEFT, false);
                    _animator.SetBool(WALLRUN_RIGHT, false);
                    break;
            }
        }
    }
}