using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = Unity.Mathematics.Random;

namespace Visuals {

    [RequireComponent(typeof(Animator))]
    public class MainMenuAnimationController : MonoBehaviour {
        
        private Animator _animator;
        private static readonly int NEXT_ANIMATION = Animator.StringToHash("NextAnimation");

        private Random _random;
        
        private void Awake() {
            _animator = GetComponent<Animator>();
            _random = new Random();
            _random.InitState();
        }

        private IEnumerator Start() {
            while (isActiveAndEnabled) {
                _animator.SetInteger(NEXT_ANIMATION, _random.NextInt(0,5));
                yield return new WaitForSeconds(1f);
            }
            
        }
    }

}