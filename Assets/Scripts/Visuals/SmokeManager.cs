using System;
using System.Collections;
using System.Reflection;
using GameProgramming.Movement;
using GameProgramming.Utility.TypeBasedEventSystem;
using UnityEngine;
using UnityEngine.VFX;

namespace Visuals {

    [RequireComponent(typeof(VisualEffect))]
    public class SmokeManager : MonoBehaviour, ITypeBasedEventHandler<MovementState> {

        private MoveSet _moveSet;

        private VisualEffect _effect;

        private bool _sliding;
        
        private void Awake() {
            _moveSet = GetComponentInParent<MoveSet>();
            _effect = GetComponent<VisualEffect>();
            _effect.Stop();
        }

        private void OnEnable() {
            _moveSet.EventSystem.RegisterHandler(this, typeof(MovementStateSliding), typeof(MovementStateJumping));
            _moveSet.GroundCheck.OnGrounded += HandleOnGrounded;
            _moveSet.GroundCheck.OnUngrounded += HandleOnUngrounded;
        }

        private void OnDisable() {
            _moveSet.EventSystem.UnregisterHandler(this, typeof(MovementStateSliding), typeof(MovementStateJumping));
            _moveSet.GroundCheck.OnGrounded -= HandleOnGrounded;
            _moveSet.GroundCheck.OnUngrounded -= HandleOnUngrounded;
        }

        public void HandleTypeBasedEventStart(MovementState instanceOfType) {
            Type type = instanceOfType.GetType();
            if (type == typeof(MovementStateSliding)) {
                _sliding = true;
                if(_moveSet.GroundCheck.Grounded) _effect.SendEvent("StartSlideSmoke");
            }
            if (type == typeof(MovementStateJumping)) _effect.SendEvent("Jump");
        }

        public void HandleTypeBasedEventEnd(MovementState instanceOfType) {
            if (instanceOfType.GetType() == typeof(MovementStateSliding)) {
                _sliding = false;
                _effect.SendEvent("StopSlideSmoke");
            }
        }
         
        private void HandleOnGrounded() {
            if(_sliding) _effect.SendEvent("StartSlideSmoke");
        }
        
        private void HandleOnUngrounded() {
            if(_sliding) _effect.SendEvent("StopSlideSmoke");
        }
        
    }
}