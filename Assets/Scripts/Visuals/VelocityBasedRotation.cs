using System;
using UnityEngine;
using Utility;

namespace Visuals {

    public class VelocityBasedRotation : MonoBehaviour {

        [Tooltip("A multiplier for the angle, the higher the more reactive")]
        [SerializeField] private float _angleAmplifier;

        [Tooltip("How fast changes in velocity are displayed")]
        [Range(0,1)]
        [SerializeField] private float _lerpPercentage = 0.75f;
        
        private Rigidbody _rigidbody;

        private Vector3 _lastVelocity = Vector3.zero;
        private Vector3 _leftOverDifference = Vector3.zero;

        private void Awake() {
            _rigidbody = GetComponentInParent<Rigidbody>();
        }

        private void FixedUpdate() {

            // Calculate current Velocity, apply the new difference to the leftOver
            Vector3 currentVelocity = _rigidbody.velocity.xOz();
            _leftOverDifference += currentVelocity - _lastVelocity;
            _lastVelocity = currentVelocity;

            // Take a fraction of the LeftOver to display it
            Vector3 displayFraction = _leftOverDifference * _lerpPercentage;
            _leftOverDifference *= (1 - _lerpPercentage);
            
            // we rotate around the vector perpendicular to the difference based on an angle
            Vector3 normal = Vector3.Cross(Vector3.up, displayFraction);
            float angle = displayFraction.magnitude * _angleAmplifier;
            
            transform.rotation = Quaternion.AngleAxis(angle, normal) * _rigidbody.rotation;
            
        }
    }

}